---
layout: handbook-page-toc
title: Support Team Handbook
description: The GitLab Support Team Handbook is the central repository for why and how we work the way we do.
---

## Welcome to the GitLab Support Team Handbook
{: .no_toc}

The GitLab Support Team provides technical support to GitLab.com and Self-Managed GitLab customers. The GitLab Support Team Handbook is the central repository for why and how we work the way we do.

| If you are | Your Need | Where You Should Look |
| ---------- | --------- | --------------------- |
| A customer, or an advocate for a customer | Technical assistance | Public [Support Page](/support), which describes the best way to get the help you need and lists GitLab's paid service offerings |
| GitLab team member | Technical assistance | [Internal Support for GitLab Team Members page](internal-support) |
| New Support Team member | Onboarding / Learning | [Support Engineer Responsibilities](/handbook/support/support-engineer-responsibilities.html) page and [Support Learning Pathways](/handbook/support/training/) |
| New Support Manager | Onboarding / Learning | [Support Manager Responsibilities](/handbook/support/managers/manager-responsibilities.html) page and [Support Manager Pathways](/handbook/support/training/#support-manager-onboarding-pathway) |

Know someone who might be a great fit for our team? Please refer them to the job-family descriptions below.

- [Support Engineering Job Family](https://about.gitlab.com/job-families/engineering/support-engineer/overview/)
- [Support Management Job Family](https://about.gitlab.com/job-families/engineering/support-management/)
- [Support Operations Job Family](https://about.gitlab.com/job-families/engineering/support-operations-specialist/)

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What does the Support Team do?

### We care for our customers

- Always assume you are the person responsible for ensuring success for the customer.
- When supporting a customer, any issue, incident or loss is _GitLab's loss_.
   - When a customer experiences trouble or downtime, take action with the same urgency you'd have if GitLab were experiencing downtime.
   - When a customer is losing productivity, take action with the same urgency you'd have if GitLab were losing productivity.
   - The rule of thumb is a customer down with 2,500 users gets the same urgency as if GitLab were losing $1,000,000 per day. This treatment is equal regardless of how much they are paying us.
- Escalate early. Visibility across GitLab, up to and including the CEO, is always better earlier rather than later. Ensure all resources needed are on the case for customers early.

Remember, as members of the support team we are the first to interact with someone when they have a problem or question. As such it is up to us to represent the company and make sure we present ourselves properly. Therefore we are expected to:

 - Always be friendly and respectful.
 - Be open to new ideas and points of view.
 - Be OK if you don't know something. You can always ask someone else.
 - Be comfortable saying no to a customer (but try to suggest a workaround and escalate to a Senior if necessary).

### Our role within GitLab

GitLab Support is part of the [Engineering division](/handbook/engineering/).
While most engineering departments are part of the R&D [cost center](/handbook/finance/financial-planning-and-analysis/#cost--reporting-structure),
Support is part of the Cost of Sales (or sometimes Cost of Goods Sold (COGS)) cost center.

This unique arrangement is expressed in our [Key Performance Indicators](/handbook/support/performance-indicators/),
which are largely focused around pursuing customer success and satisfaction
while driving efficiency by increasing output while keeping costs within a
predefined range.

This is also why [Support Engineer responsibilities](/handbook/support/support-engineer-responsibilities.html)
include contributing code and documentation and working alongside Product
Managers on our products: By using the knowledge gained from interacting with
our customers to make our products or docs better, we solve problems before
they become one. This reduces support case load while increasing efficiency for
the wider GitLab organization. For example, the Sales department can rely on
our docs to answer customer queries instead of leaning on Support or Customer
Success for help, freeing up more time to close sales.

#### Building customer empathy

Part of Support's role is to amplify the voice of the customer. One way of doing this is inviting other GitLab team members into
experiences that will help them understand customer challenges with the product or our own obstacles in helping customers overcome those challenges.

Before you start, make sure you get [light-agent access in Zendesk](/handbook/support/internal-support/#viewing-support-tickets) so that you can view Support tickets.

If you're looking to get more exposure to customers, there are a few ways to get involved with Support:

##### Support Shadow Program

GitLab team members interested in learning about the GitLab Support team and our responsibilities are encouraged to participate in the Support Shadow Program. The Support Shadow Program is a way that team members outside of Support can spend time shadowing, learning, collaborating, and working together with the GitLab Support team.

To participate in this program, open [a Support Shadow Program issue](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/new?issuable_template=Support%20Shadow%20Program) in the `support-team-meta` project. This issue will be used to organize, plan, and track progress toward this program.

##### Join Support Calls

Support calls are published on the [GitLab Support Calendar](#google-calendar). There are:
- **customer calls** between customers and engineers. The description will include a ticket ID. Go to `https://gitlab.zendesk.com/agent/tickets/<id>` to view the ticket.
- **pairing sessions** between two or more engineers working on one or more tickets.
- **office hours / help sessions** where more experienced Support engineers share their knowledge.
- **training sessions** where a member of the support team is presenting on a specific topic

##### Join an Emergency Call

A great way to get involved is to join a customer emergency call. You can monitor `#support_self-managed` for PagerDuty alerts. Alternatively,
if you have access to PagerDuty you can be scheduled into a [shadow rotation](/handbook/support/on-call/#your-first-on-call-shift).


### How our team helps fellow team members at all levels -- Helping Hierarchy

If you go through the responsibilties for each role in Support you can piece together how the organization works. We wanted to make a simple clear way to think about how the roles work together to solve problems:

- Support Engineers help solve customer problems via tickets, merge requests, and other customer facing activities
- Managers help solve Support Engineer problems by removing obstacles, joining in on customer facing activities, and working with support engineers to build systems that work to reduce friction and enable results and efficiency.
- Senior Managers help resolve and avoid scaling problems by addressing team performance to KPIs, prioritizing initiatives and being responsible for the achievement of global results.
- VP of Support helps resolve and avoid company wide problems, by identifying growth and team design challenges, and reporting on progress to the Executives and Board.

This simple list helps to give an easy way to set expecations and align problem solving in different roles.


### How we measure our performance

We use [Key Performance Indicators](/handbook/support/performance-indicators/) (KPIs) to keep track of how well each Engineering Department is doing, including the Support team, as a whole.

The KPI measurements can be found under the `Reporting` tab in Zendesk if you have the appropriate access, but progress on meeting these KPIs is also tracked via the aforementioned KPI link and visually through reports in the [Support KPIs Dashboard](https://app.periscopedata.com/app/gitlab/421422/).

We review these KPIs weekly in the [Support Week-in-Review](/handbook/support/#support-week-in-review).

### About the Support Team

The support team has a few elements and we've divided the support handbook as such:

- [/support/engineering](/handbook/support/engineering/index.html) is content that is for Support Engineers. Think: Zendesk workflows and technical resources.
- [/support/managers](/handbook/support/managers/index.html) is content that is for Support Managers. Think: how to manage issues, run 1:1s and leadership sync information.
- [support/support-ops](/handbook/support/support-ops/index.html) is content that is for Support Operations. Think: how to change Zendesk forms and fields, and other ops details.

Below we also have some commonly referenced pages:

- [Support Engineer responsibilities](/handbook/support/support-engineer-responsibilities.html)
- [Support Engineer knowledge areas](/handbook/support/workflows/knowledge_areas.html)
- [Support Engineer career path](/handbook/support/support-engineer-career-path.html)
- [Support Manager responsibilities](/handbook/support/managers/manager-responsibilities.html)


---

## FY23 Direction

Gitlab Support's vision is to deliver a consistent, exceptional experience to all customers across the globe. Our regional teams will collaborate and act as one team to deliver on that vision by hiring exceptional talent, developing that talent through quality training, and refining workflows to provide a smooth experience for engineers and customers alike.

During GitLab's fiscal year 2022 (FY22), GitLab support began the task of adapting and enhancing support processes to keep pace with our customers' expectations plus addressing the additonal considerations presented by a near 140% expansion of the team over a two year period to the start of GitLab's fiscal year 2023 (FY23).

The overall direction for Support in FY23 will continue building from the foundations laid in FY22. With focus on KPI achievement via collaboration and efficiency, and continuing to evolve what and how we deliver service to GitLab customers to support the company's overall [strategic objectives](/strategy), with a particular emphasis on;

- Customer Results
- Team Member Development
- Organizational Efficiency 

While our [publicly visible OKR page](/company/okrs/) and [Key Performance Indicators](https://about.gitlab.com/handbook/support/performance-indicators/#key-performance-indicators) reflect the focus and progress for the current quarter, the following provides more detail on the items included in the themes for the entire FY23.

### Customer Results

The expanding customer license base in tandem with the expansion of our Global Support team presents an opportunity to revisit many aspects of our daily work and look for improvement areas of benefit to our customers. Specifically in FY23 we will:

- Review and refine the management of a ticket from first repsonse to closure. Giving consideration to meeting SLA/SLOs, ticket ownership and responsibilities, timely communication and setting and meeting expectations whilst transferring a ticket around the global team when priority dictates.
- Review and enhance internal processes to provide a clear view to the team of current support priorities to enable efficent working on internal issues.
- Increase contributions to product and documentation based on support learnings to directly share the knowledge our support team gains from hearing about our customers' product experience.
- Enhance customer facing and internal support systems as is necessitated by any process change.

### Team Member Development

The Support Team, Product areas and requirements of our customers have continued to grow in FY22. To support this growth and with the end goal of providing an excellent customer support experience, career growth opportunities and personal improvement, in FY23 we will: 

- Improve upon existing training/training materials with particular awareness of not only technical skills but also complementary skills to enable team members to feel confident and equipped to perform their roles.
- Foster a culture where global activities, continuous improvement and learning are key elements of what it means to be a support professional at GitLab and appropriate time in the working week can be dedicated to these areas of growth.
- Continue to review the career progression path for our support professionals.

### Organizational Efficiency

The growth of the support team provides an opportunity to review its structure and the role each team member plays to contribute to the success of the team and achieving the [Key Performance Indicators](https://about.gitlab.com/handbook/support/performance-indicators/#key-performance-indicators). To aid in success and achievement in FY23 we will:

- Review and, where required, refresh the messaging behind our chosen [Key Performance Indicators](https://about.gitlab.com/handbook/support/performance-indicators/#key-performance-indicators)
- Build out a Support Readiness team who will focus on equipping/training Support Engineers and Managers while influencing company structures to the end of providing an excellent customer experience.
- Create operational efficiency and reduce error rates by building tooling that mechanizes or automates engineer workflows.

---

<%= partial "includes/we-are-also-product-development.md" %>

## OKRs

### Current Quarter
<iframe src="https://app.ally.io/public/oMkZTFwfFk3lfwH" class="dashboard-embed" style="border:none;" width="100%" height="1700"> </iframe>

### Previous Quarter
<iframe src="https://app.ally.io/public/KGte77yvEt30BS9" class="dashboard-embed" style="border:none;" width="100%" height="1700"> </iframe>

## Hazards and Challenges

See [Managers/Hazards page](/handbook/support/managers/hazards-and-challenges.html)

## Communications

The GitLab Support Team is part of the wider Engineering function. Be sure to check the
[communications section in the Engineering handbook](/handbook/engineering/#communication)
for tips on how to keep yourself informed about engineering announcements and initiatives.

Here are our most important modes of communication:

- [Support Week in Review](#support-week-in-review). Important updates for everyone in support.
You should try to check the SWIR at least once a week. If you have something to share with the
entire team this is the best place to do it. For example, if you have an issue for a common bug, an issue that requires feedback,
or an issue about an external project you're working.
- [Slack channels](#slack) for ["informal"](/handbook/communication/#slack) communication.
Due to our data retention policy in Slack, things shared there will eventually be deleted. If you want to share something there, please make sure it also has a more permanent place in our docs, handbook, issue tracker, etc.
- [Meta issue tracker](https://gitlab.com/gitlab-com/support/support-team-meta/issues/) for any issues regarding workflows,
general team suggestions, tasks or projects related to support, etc.

Where we want to ensure that important messages are passed to the global support team, we will use this [messaging template](https://gitlab.com/gitlab-com/support/managers/leadership-sync/-/blob/master/.gitlab/issue_templates/support-message-plan.md). This ensures that these messages are delivered across our communications channels in a structured and documented manner.

### GitLab.com

#### Groups
{: .no_toc}

We use the following GitLab Groups to notify or add support team members to issues and merge requests on
GitLab.com.

| Group | Who |
| ----- | --- |
| [@gitlab-com/support](https://gitlab.com/gitlab-com/support) | All Support Team members |
| [@gitlab-com/support/amer](https://gitlab.com/gitlab-com/support/amer) | AMER Support |
| [@gitlab-com/support/apac](https://gitlab.com/gitlab-com/support/apac) | APAC Support |
| [@gitlab-com/support/emea](https://gitlab.com/gitlab-com/support/emea) | EMEA Support |
| [@gitlab-com/support/dotcom](https://gitlab.com/gitlab-com/support/dotcom) | Support members with primary SaaS focus and GitLab.com admin access |
| [@gitlab-com/support/dotcom/console](https://gitlab.com/gitlab-com/support/dotcom/console) | Support members with GitLab.com console access |
| [@gitlab-com/support/customers-console](https://gitlab.com/gitlab-com/support/customers-console) | Support members with CustomersDot console access |
| [@gitlab-com/support/licensing-subscription](https://gitlab.com/gitlab-com/support/licensing-subscription) | Support members focused on License and Renewals |
| [@gitlab-com/support/managers](https://gitlab.com/gitlab-com/support/managers) | All support managers |

#### Projects
{: .no_toc}

Our team projects and issue trackers can be found in the [Support parent group](https://gitlab.com/gitlab-com/support). Here are some selected projects
which are relevant to team communications.

| Project | Purpose |
| ------- | ------- |
| [support-team-meta](https://gitlab.com/gitlab-com/support/support-team-meta) | Issues to discuss and improve Support processes |
| [support-training](https://gitlab.com/gitlab-com/support/support-training) | Courses and training for Support team including onboarding |
| [support-pairing](https://gitlab.com/gitlab-com/support/support-pairing) | Record of pairing sessions where we work together on tickets |
| [feedback](https://gitlab.com/gitlab-com/support/feedback) | Collects SSAT survey responses from Zendesk in the form of issues |
| [support-operations](https://gitlab.com/gitlab-com/support/support-ops/support-ops-project) | Support Operations team project |

##### Support team meta issue tracker

We use the [Support meta issue tracker](https://gitlab.com/gitlab-com/support/support-team-meta/issues) for tracking issues
and creating issues that may require feedback around support.

If you're interested in working on a project or task related to
support feel free to create an issue and link to any external issues or projects so that we can:

- Be transparent to the entire team what we're working on
- Have the opportunity to collaborate on external projects or tasks with other team members who are interested
- Avoid having team members do duplicate work

Issues regarding documentation or features for GitLab, our FOSS project or any of the GitLab
components should not go in this issue tracker, but in their appropriate issue tracker.

If you have a proposed solution that is actionable, it's best to [start a merge request](/handbook/communication/#everything-starts-with-a-merge-request), tag the team for feedback and link in the [Support Week in Review](#support-week-in-review).

### Slack

We follow GitLab's [general guidelines for using Slack](/handbook/communication/#slack)
for team communications. As only 90 days of activity will be retained, make sure
to move important information into the team handbook, product documentation,
issue trackers or customer tickets.

#### spt_ vs. support_ prefix
{: .no_toc}

When naming channels, "spt" is meant for internal channels, meaning those that will be of use to the Support Team mainly. They should be public so others may join if they choose. If a channel has a "support" prefix, it is meant as a public interface where other teams will interact with the Support Team.

The "spt_gg_" prefix is used for [Support Global Groups](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/3879) internal channels.

#### Channels
{: .no_toc}

| Channel | Purpose |
| ------- | ------- |
| [#support_team-chat](https://gitlab.slack.com/archives/CCBJYEWAW) | Support team lounge for banter, chat and status updates |
| [#support_gitlab-com](https://gitlab.slack.com/archives/C4XFU81LG) | Discuss GitLab.com tickets and customer issues |
| [#support_self-managed](https://gitlab.slack.com/archives/C4Y5DRKLK) | Discuss self-managed tickets and customer issues |
| [#support_escalations](https://gitlab.slack.com/archives/CBVAE1L48) | Discuss escalated tickets with the Support Manager On-Call. |
| [#spt_managers](https://gitlab.slack.com/archives/C01F9S37AKT) | Discuss support team internal matters which require support managers' attention |
| [#spt_hiring](https://gitlab.slack.com/archives/CE9S6JW4S) | Discuss support team hiring-related matters |

#### User Groups
{: .no_toc}

| Group | Who |
| ----- | --- |
| `@support-dotcom` | Support Team Members with GitLab.com Admin Access |
| `@support-selfmanaged` | Support Team focused on Self-Managed tickets |
| `@support-team-apac` | Support Team APAC |
| `@support-team-emea` | Support Team EMEA |
| `@support-team-americas` | Support Team AMER |
| `@supportmanagers` | Support Managers |
| `@support-managers-apac` | Support Managers APAC |
| `@support-managers-emea` | Support Managers EMEA |

If you need to be added to one or more of these groups, please open an issue in
the [access requests project](https://gitlab.com/gitlab-com/access-requests).

### Google Calendar

We use the following team calendars to coordinate events and meetings:

- [GitLab Support](https://calendar.google.com/calendar/embed?src=gitlab.com_9bs159ehrc5tqglur88djbd51k%40group.calendar.google.com) Calendar ID `gitlab.com_9bs159ehrc5tqglur88djbd51k@group.calendar.google.com`
- [Support - Time off](https://calendar.google.com/calendar/embed?src=gitlab.com_as6a088eo3mrvbo57n5kddmgdg%40group.calendar.google.com) Calendar ID `gitlab.com_as6a088eo3mrvbo57n5kddmgdg@group.calendar.google.com`

Add these calendars to your GitLab Google calendar by clicking on the "+" sign next to "other calendars", and choose "subscribe to calendar". Enter the relevant ID mentioned above. If you need access to these calendars, ask a support team member for help.

### Weekly Meetings

The Support Team has several meetings each week. These allow us to coordinate and
help us all grow together. Each meeting has its own agenda and is led by a different member of the team each week.

Discussions are encouraged to be kept in issues or merge requests so the entire team can collaborate, regardless of time zone.

Any demos or announcements that need to be shared with the entire team should be shared in the [Support Week in Review](#support-week-in-review).

| Weekday | Region | Meeting Name | Purpose |
| :-----: | :----: | :----------: | :-----: |
| Tuesday | APAC | Support Call | Discuss metrics, demos, upcoming events, and ask questions |
| Tuesday | AMER | Support Call | Semi-structured agenda to welcome new members, discuss meta issues, get thoughts on tickets, socialize |
| Thursday | EMEA | Support Call | Discuss metrics, demos, upcoming events, and ask questions |

The regions listed above are the regions for which each call may be the most convenient, but all are welcome on any call. Every call is recorded and notes are taken on the agenda for each one. If you miss a meeting or otherwise can't make it, you can always get caught up.

All Zoom and agenda links can be found on the relevant calendar entry in the Support Calendar.

#### Support Leadership Meetings

The Support management team meet regularly. Details of these calls are on the [Support Managers page](/handbook/support/managers)

#### Support Regional Team Meetings

Some regional Support teams have meetings oriented around company news, Support initiatives, training plans, and connectedness.

| Weekday | Region | Meeting Name | Purpose |
| :-----: | :----: | :----------: | :-----: |
| Wednesday | AMER-E | Weekly News | For team members reporting to Rebecca S |

#### Senior Support Engineer Office Hours

Senior and Staff Support Engineers are encouraged to host office hours. These office hours are intended to strengthen
the team through mentoring. It is up to each Senior/Staff Support Engineer whether they schedule office hours, and how
often. Please see the "GitLab Support" Team calendar to view office hours and invite yourself.

We encourage hosts to include what they will cover in the calendar event description and optionally a document to track.

Some ideas of what one can expect at a Senior/Staff Support Engineers'

- Troubleshooting a difficult ticket
- Trying out a GitLab feature (Geo, CI, SAST, k8s, etc.) or a new workflow
- Reproducing a particular bug
- Fixing a bug
- Creating or updating documentation
- Thinking through a particular problem
- Hosting a ticket crush session

#### Meeting Roles

##### Role of the Chair
{: .no_toc}

The main role of the chair is to start the meeting, keep the meeting moving along, and end the meeting when appropriate. There is generally little preparation required, but depending on the meeting, the chair will include choosing a "feature of the week" or similar. Please check the agenda template for parts marked as "filled in by chair."

During the meeting, the chair:

- will ensure that each point in the agenda is covered by the listed person,
- may ask the team to move a discussion to a relevant issue when appropriate,
- copy the agenda template for the following week and tag the next chair/secretary.

If a chair is not available, it is their responsibility to find a substitute.

##### Role of the Notetaker
{: .no_toc}

The notetaker should take notes during the meeting and if action is required, creates a comment and assigns it to the appropriate person.

If the notetaker is not available when it is their turn, they should find a substitute.

### Support Week in Review

Every Friday, we do a week in review, inspired by the [greater Engineering organization week in review](https://drive.google.com/drive/u/0/search?q=type:document%20title%20%22Engineering%20week-in-review%22).  You can add topics any time to the [support week in review google document](https://drive.google.com/drive/u/0/search?q=%22Support%20Week%20in%20Review%22%20-archive%20-Senior%20parent:1eBkN9gosfqNVSoRR9LkS2MHzVGjM5-t5).

Any workflow changes or announcements should be shared in the SWIR and we recommend you check at least once a week to stay up to date on recent changes.
Ideally, the information shared here should have a permanent location such as an issue or merge request.

We encourage anyone in the team to share. We currently have the following topics:

- **Actionable**. For items that require a decision to be made or action to be taken (such as, asking for feedback on an issue).
- **Kudos**. Give a special kudos to other team members or highlight something they did.
- **Things to know about**. Share items that you would like to share with the team, like projects you're working on, known bugs, new workflows, cool articles you found, etc.
- **Metrics report**. Review the support metrics for the span of the week.
- **Music/TV/Movie Sharing**. Share and recommend a song, a show, or a movie with the team.
- **Personal Updates / Weekend Plans / Random**. Share awesome pics from recent trips, what you're up to on the weekend, or something else.

You can read about how we got this started
[in this issue](https://gitlab.com/gitlab-com/support/support-team-meta/issues/1394).

### Cross-Department Roles

The Support Team collaborates with many other departments throughout GitLab -
Sales, Channel, Product and Legal, to name a few. And we have created two
different roles to help those collaborations to be as effective and efficient
as possible.

#### Support Stable Counterpart

The Support Stable Counterpart role is designed to provide a strong connection
between a product team and Support for the purpose of discussing product issues,
sharing product knowledge and representing customer needs. If you are interested
in becoming a Support Stable Counterpart, or would like to learn more about the
role, read the
[Support Stable Counterparts](/handbook/support/support-stable-counterparts.html)
page.

#### Support Liaison

The Support Liaison role is designed to provide a strong relationship between
a non-product team and Support for the purpose of sharing knowledge
about each team's work and of developing processes and documentation to allow
the two teams to work together well. If you are interested in becoming a
Support Liaison, or would like to learn more about the role, read the
[Support Liaisons](/handbook/support/support-liaisons.html) page.

## Processes

### Updating Support Team documents using merge requests

The Support Team records our institutional knowledge, processes and workflows
in multiple places, such as the handbook and project issues templates. When
updating such documents, make sure to have visible artifacts of approval on
your merge requests before merging even if you have received approval somewhere
else. This avoids the impression of changes being made without any oversight
or accountability.

Artifacts of approval can include:

- Getting a peer or manager to review and merge your MR
- A peer or manager showing their approval using [MR approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- A peer or manager commenting "looks good to me"

### Support Workflows

- [Support Workflows](/handbook/support/workflows)
    - [Internal Policies and Procedures Wiki](https://gitlab.com/gitlab-com/support/internal-requests/-/wikis/home)
    - [How to Work with Tickets](/handbook/support/workflows/working-on-tickets.html)
    - [How to Submit issues to Product/Development](/handbook/support/workflows/working-with-issues.html)
    - [How to Submit Code to the GitLab Application](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md)
    - [How to Submit Docs when working on customer issues](/handbook/documentation) (see 'docs-first methodology')
- [License & Renewals Workflows](/handbook/support/license-and-renewals/workflows)

### Slack Workflows

Each Slack channel within Support has a number of [Workflows](https://slack.com/help/articles/360035692513-Guide-to-Workflow-Builder) attached to them that are used to provide information to users. The source files for each workflow live in the [slack-workflows](https://gitlab.com/gitlab-com/support/toolbox/slack-workflows) project.

#### Issue Notification

Some workflows are meant to notify the team of new issues created in the relevant project.
In these cases, a [project webhook](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html) passes information to [Zapier](https://zapier.com/app/zaps/folder/210292),
which then sends the information to a Slack workflow.

- `#support_gitlab-com`
    - CMOC [Gitlab Project](https://gitlab.com/gitlab-com/support/dotcom/cmoc-handover/), [Zap](https://zapier.com/app/zap/100087156), [Slack workflow](https://gitlab.com/gitlab-com/support/toolbox/slack-workflows/-/blob/master/cmoc_handover.slackworkflow)
- `#support_licensing-subscription`
    - L&R related internal requests [Gitlab Project](https://gitlab.com/gitlab-com/support/internal-requests/), [Zap](https://zapier.com/app/zap/98925072), [Slack workflow](https://gitlab.com/gitlab-com/support/toolbox/slack-workflows/-/blob/master/support_licensing_subscription_internal_requests.slackworkflow)

#### Emoji Reaction

Providing information by reacting to a message with a specific emoji.

- `#support_escalations`
    - [Ticket Escalation](https://gitlab.com/gitlab-com/support/toolbox/slack-workflows/-/blob/master/support_managers_ticket_escalation.slackworkflow) - `:escalate:` - Directs the user to follow the proper procedure to escalate a support ticket or internal issue.
- `#support_gitlab-com`
    - [Ticket Escalation](https://gitlab.com/gitlab-com/support/toolbox/slack-workflows/-/blob/master/support_gitlab_com_ticket_escalation.slackworkflow) - `:escalate:` - Directs the user to follow the proper procedure to escalate a support ticket or internal issue.
    - [Question Redirect](https://gitlab.com/gitlab-com/support/toolbox/slack-workflows/-/blob/master/support_gitlab_com_question_redirect.slackworkflow) - `:leftwards_arrow_with_hook:` - Directs the user to post their question in a more appropriate Slack channel.
    - [Remove Link Preview](https://gitlab.com/gitlab-com/support/toolbox/slack-workflows/-/blob/master/support_gitlab_com_remove_link_preview.slackworkflow) - `:slack:` - Politely asks the user to remove any unfurled link previews in their message.
    - [Welcome](https://gitlab.com/gitlab-com/support/toolbox/slack-workflows/-/blob/master/support_gitlab_com_welcome.slackworkflow) - This automated workflow automatically sends a direct message to new members of the channel that contains helpful information.
- `#support_self-managed`
    - [Ticket Escalation](https://gitlab.com/gitlab-com/support/toolbox/slack-workflows/-/blob/master/support_self_managed_ticket_escalation.slackworkflow) - `:escalate:` - Directs the user to follow the proper procedure to escalate a support ticket or internal issue.
- `#support_licensing-subscription`
    - [Ticket Escalation](https://gitlab.com/gitlab-com/support/toolbox/slack-workflows/-/blob/master/support_licensing_subscription_ticket_escalation.slackworkflow) - `:escalate:` - Directs the user to follow the proper procedure to escalate a support ticket or internal issue.

### Time Off

_See the [Support Time Off page](/handbook/support/support-time-off.html)_

### Onboarding

_See the [Support Onboarding page](/handbook/support/training)_

### Improving our processes - 'Active Now' issue board

The Support team use ['support-team-meta' project issues](https://gitlab.com/gitlab-com/support/support-team-meta/issues/) to track ideas and initiatives to improve our processes. The ['Active Now' issue board](https://gitlab.com/gitlab-com/support/support-team-meta/-/boards/580661) shows what we're currently working on. It uses three labels:

1. **Blocked** - waiting for another team or external resource before we can move ahead
1. **Discussing this week** - under active discussion to arrive at a decision
1. **In Progress** - actively being worked on

Some principles guide how these labels are used:

1. A **maximum of six issues** at any time for each label (18 total issues)
1. All issues with one of the above labels must be **assigned** to one or more support team members
1. All issues with one of the above labels must have a **due date** no longer than a week ahead
1. If an issue is too big to complete in a week it should be **split into smaller parts that can be completed in a week** (a larger 'parent' issue is OK to keep in the project, but it shouldn't make it onto the 'In Progress' column)

**Each week we look at the board and discuss the issues to keep things moving forward.**

By keeping a maximum of six issues for each label, we **limit work in progress** and make sure things are completed before starting new tasks.

**Adding and managing items on the board:**

Support managers will regularly review the board to keep items moving forward.

1. The team can **vote on issues not on the board** by giving a 'thumbs up' emoji so we can see [popular issues](https://gitlab.com/gitlab-com/support/support-team-meta/issues?sort=popularity&state=opened).
1. Support managers will look at popular issues and add them to the board when there is room.
1. Support managers will **curate** issues to prevent a large backlog. Unpopular or old issues can be closed / merged to keep the backlog manageable.

#### Support Slackbot

The [Support Slackbot (archived)](https://gitlab.com/gitlab-com/support/toolbox/gitlab-support-bot) has been retired.

## <i aria-hidden="true" class="fas fa-book fa-fw icon-color font-awesome"></i>Support Resources

### Handbook links

- [GitLab Team page](/company/team/)
- [Product Categories](/handbook/product/categories/) - Find out what team handles what
- [Statement Of Support](https://about.gitlab.com/support/statement-of-support.html)
- [Support Managers](/handbook/support/managers/)
- [Support Hiring](/handbook/support/managers/hiring.html)
- [Support Channels](/handbook/support/channels/)
- [On-Call](/handbook/on-call/)
- [License & Renewals](/handbook/support/license-and-renewals/)
- [Support Ops](/handbook/support/support-ops/)
- [Advanced Topics](/handbook/support/advanced-topics/)

### Documentation

- GitLab
    - [GitLab.com Status](https://status.gitlab.com/)
    - [GitLab Releases](/releases/categories/releases/)
- Writing docs
    - [GitLab Documentation guidelines](https://docs.gitlab.com/ee/development/documentation/index.html)
    - [Documentation Style Guide](https://docs.gitlab.com/ee/development/documentation/styleguide/)
    - [GitLab Markdown](https://docs.gitlab.com/ee/user/markdown.html)
- Setting up GitLab
    - [GitLab Architecture Overview](https://docs.gitlab.com/ee/development/architecture.html)
    - [Requirements](https://docs.gitlab.com/ee/install/requirements.html)
    - [Installation methods for GitLab](/install/)
    - [Backing up and restoring GitLab](https://docs.gitlab.com/ee/raketasks/backup_restore.html)
    - [Omnibus configuration settings](https://docs.gitlab.com/omnibus/settings/README.html)
    - [Omnibus Configuration options](https://docs.gitlab.com/omnibus/settings/configuration.html)
    - [Omnibus Database settings](https://docs.gitlab.com/omnibus/settings/database.html#seed-the-database-fresh-installs-only)
- Debugging GitLab
    - [Log system](https://docs.gitlab.com/ee/administration/logs.html)
    - [Rake tasks](https://docs.gitlab.com/ee/raketasks/README.html)
    - [Maintenance Rake Tasks](https://docs.gitlab.com/ee/administration/raketasks/maintenance.html)
    - [Debugging Tips](https://docs.gitlab.com/ee/administration/troubleshooting/debug.html)
    - [Debugging resources for GitLab Support Engineers](https://docs.gitlab.com/ee/administration/index.html#support-team-docs)
    - [GitLab Rails Console Cheat Sheet](https://docs.gitlab.com/ee/administration/troubleshooting/gitlab_rails_cheat_sheet.html)
- GitLab features
    - [Install GitLab Runner](https://docs.gitlab.com/runner/install/)
    - [GitLab CI example projects](https://gitlab.com/gitlab-examples)
    - [Elasticsearch](https://docs.gitlab.com/ee/integration/elasticsearch.html)
    - [Connecting GitLab with a Kubernetes cluster](https://docs.gitlab.com/ee/user/project/clusters/)
- Developing GitLab
    - [GitLab development utilities](https://docs.gitlab.com/ee/development/utilities.html)
    - [Feature flags](https://docs.gitlab.com/ee/development/feature_flags/index.html)
    - [What requires downtime?](https://docs.gitlab.com/ee/development/what_requires_downtime.html)

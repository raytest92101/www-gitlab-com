---
layout: handbook-page-toc
title: GitLab Company-Wide Mentorship Program
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## GitLab Company-Wide [Mentorship](https://about.gitlab.com/handbook/people-group/learning-and-development/mentor/) Program

The Learning and Development team is hosting the first company-wide mentorship program in FY23 Q1. It's inspired by the success of the [Minorities in Tech Mentoring Program](/company/culture/inclusion/erg-minorities-in-tech/mentoring/) and the [Women at GitLab mentorship program](/company/culture/inclusion/tmrg-gitlab-women/mentorship-program/). Included in this program is an [opt-in async-first option.](/handbook/people-group/learning-and-development/mentor/company-program/#async-first-option)

Learn more about the purpose and results of mentorship on the [mentor handbook page.](/handbook/people-group/learning-and-development/mentor/#what-is-mentoring)

## What does the program look like?

### Program structure

Review the program structure below to understand what is expected of mentees and mentors who apply for this program:

| Program Title | Description |
| ----- | --------------- |
| **Initial program kickoff** | 25-minute discussion pannel with mentors, mentees, and company leadership held at multiple time zones. Attendance is **required** and the sessions will be recorded. |
| **Mentorship Sessions** | Mentors and mentees meet sync or async every other week for 30-minutes for 5 months (with the option to extend). Sessions are led by the mentee and should be focused on specific goals. Participation is **required**.|
| **Mentor/Mentee Workbook** | [Mentorship resources](/handbook/people-group/learning-and-development/mentor/#resources) are available for all mentor/mentee pairs, including suggested articles, strategies for goal setting, and sample meeting agendas. Resources use is **encouraged** but not required. |
| **Mentor/Mentee Training** | Completion of the self-paced [How to be a Good Mentor or Mentee](https://www.linkedin.com/learning/how-to-be-a-good-mentee-and-mentor/the-power-of-mentoring?u=2255073) course on LinkedIn Learning is **required** of all mentors and mentees. If you prefer text-based learning or don't have a LinkedIn Learning license, you can instead review [these slides](/handbook/people-group/learning-and-development/mentor/#mentor-and-mentee-training) based on the LinkedIn course. |
| **Monthly Office Hours** | Monthly office hours are held by volunteers in the program. Office hours will be 25-minute long and provide space for mentors and mentees to network, ask questions, and update their professional developpment plans. Attendance is **optional**. When mentorship pairs are created, the L&D team will ask for volunteers from different timezones to host these sessions. Hosting the session is a great leadership opportunity for both mentors and mentees. If you're interested in being an office hours host, please reach out to the L&D team in Slack. |
| **End of Program Celebration** | A live session will be hosted at multiple time zones at the end of the program. The call will be a small group discussion format and run for 25 minutes. |


### Async First Option

This program includes an opt-in, async-first structure to increase opportunity for team members in any time zone to pair with mentors company-wide. Async-first means that mentees can pair with mentors in any time zone. Both mentor and mentee applications will have a question asking if the applicant is interested and willing to be paired with an async-first mentee.

This is a pilot to test the success of async-first mentorship at Gitlab. The test will help inform how we design future mentorship programs. 

Async first pairs should consider making the following adjustments to the program structure above:

1. Connect every other week using a GitLab issue, Google doc, Slack messages or voice memos, and/or Loom recorded videos. Async does not have to mean text only.
1. Consider finding 1-3 times over the course of the full program to meet synchrousnouly, even if just for 15 minutes. This will require advance planning for both mentor and mentee and is not required.
1. Mentors and mentees should attend sync kickoff, end of program events, and office hours sessions hosted in your time zone. These sync sessions are opportunites for social learning and connection through the program and provide balance to the async-first strcuture.
1. Utilize all async resources provided on the [mentorship](/handbook/people-group/learning-and-development/mentor/#resources) handbook page.

## Current Program Timeline

Review the program timeline below for the next iteration of this program which will run from March through July of 2022

| Date | Description | Related Resources |
| ----- | ----- | ----- |
| 2021-12 | Begin program planning | [Planning Issue](https://gitlab.com/gitlab-com/people-group/learning-development/mentorship/-/issues/3)  |
| 2021-12-20 | Share interest form | [Interest form for team members](https://docs.google.com/forms/d/e/1FAIpQLSfIkmpU7YTQpoYexzZJNRjpj995t1gY3lYBPtZvq5zyi1lC2w/viewform?usp=sf_link) |
| 2022-01-31 | Call for Mentors | [Application Link](https://docs.google.com/forms/d/e/1FAIpQLSf6Xbsbu5J_vjxUy5_hu2RJ_bJ5kqcqY8qj9F7JQn8VyBo6jw/viewform?usp=sf_link) |
| 2022-01-31 | Call for Mentees | [Application Link](https://docs.google.com/forms/d/e/1FAIpQLSe_MvZX3FNEnXEaR8Cgrb5RPK_645Ra90C1GDu51cOA3QcTlQ/viewform?usp=sf_link) |
| 2022-02-11 | Applications Close | |
| On or before 2022-03-04 | Pairing of mentor relationships complete and communicated to applicants | |
| Week of 2022-03-07 | Mentor/Mentee pre-program coffee chats | |
| Week of 2022-03-07 | [Initial program Kickoff meeting](/company/culture/inclusion/tmrg-gitlab-women/mentorship-program/#program-structure) | |
| Week of 2022-07-25 | End of program celebration | Exact date TBD |


## Being a Mentor

As a mentor, you benefit from:

- Fast feedback loop between team members and leadership
- Opportunity to form relationships with team members in other departments
- Opportunity to support Women at GitLab and live our values

### Mentor requirements

- You have been with GitLab for at least 3 months at the time of application 
- You have availability in your schedule to meet sync or async with your mentee on a bi-weekly basis for at least 30 minute meetings for the entire length of the program
- You plan to make every effort to attend all live trainings included in the program structure
- You are not on a [Formal Coaching plan](/handbook/leadership/underperformance/#options-for-remediation) or PIP (Performance Improvement Plan)
- You can complete the [DIB training certification](https://gitlab.edcast.com/journey/ECL-5c978980-c9f0-4479-bf44-45d44fc56d05) before the program begins

**Applications will be prioritized based on:**

- Skill and prior experience as a mentor
- Performance in current role
- Available and appropriate mentee match in the current session

### Apply

[Apply Here](https://docs.google.com/forms/d/e/1FAIpQLSf6Xbsbu5J_vjxUy5_hu2RJ_bJ5kqcqY8qj9F7JQn8VyBo6jw/viewform?usp=sf_link)

_Applying for a mentor does not guarantee you a spot in the program and eligibility will be evaluated based on criteria above._

If you've participated in a previous GitLab mentor program, you are welcome to apply again!

## Being a Mentee

As a mentee, you benefit from:

- Increased visibility with leadership
- Increased professional development opportunities
- Career coaching and guidance
- Opportunity to form relationships with leaders on other teams

### Mentee requirements

- You have been with GitLab for at least 3 months at the time of application 
- You have availability in your schedule to meet sync or async with your mentor on a bi-weekly basis for at least 30 minute meetings for the entire length of the program
- You plan to make every effort to attend all live trainings included in the program structure
- You are not on a [Formal Coaching plan](/handbook/leadership/underperformance/#options-for-remediation) or PIP (Performance Improvement Plan)

Applications will be prioritized on many points including:

- Team members on a management career track
- Performance in current role

### Apply

[Apply Here](https://docs.google.com/forms/d/e/1FAIpQLSe_MvZX3FNEnXEaR8Cgrb5RPK_645Ra90C1GDu51cOA3QcTlQ/viewform?usp=sf_link)

_Applying for a mentee does not guarentee you a spot in the program, and eligibility will be evaluated based on criteria above._


## Metrics and Outcomes

Mentors and mentees will complete a pre-program and end of program survey to collect feedback, assess skills, and determine if goals set during mentor/mentee relationships have been set.

Success of this program will be measured by:

1. percentage of mentor/mentee pairs who meet consistently on a bi-weekly basis, with a goal of 95% completion
1. attendance to live training sessions, with a goal of 95% attendance
1. quality of feedback of the program from both the mentor and mentee perspective
1. percentage of mentee goal achieved during mentorship, as reported by mentees with a goal of at least 50% of goal achieved
1. mentor and mentee career development at GitLab after program conclusion including rates of promotion, transfer, performance growth factors, and retention


## FAQ

**Can I participate as both a mentor and a mentee?**

Yes, this is a possibility. It's important that team members talk with their managers to discuss their capacity to participate in both roles in this program. Participation in both roles will require a 2hr commitment per month (two 30 minute sessions as a mentor and two 30 minute sessions as a mentee).

It's possible that we will have a shortage of mentors and be unable to complete all mentor/mentee pairs. If there is a shortage of mentors and you've applied as both a mentor and a mentee, you will be assigned **only** as a mentor in this program. 

**I'd like to participate but I can't commit the entire length of the program. Can I still apply?**

If you cannot commit to the 5 month program, please consider applying for a future mentorship program at GitLab. It's important that both mentors and mentees are available for the full 5 month program to achieve the best results.

**I'm not eligible to be a mentor or a mentee in this program. What are my options for future participation?**

Team members are always encouraged to build mentor/mentee relationships independently. You can find resources and a list of available mentors in the [Learning and Development handbook](/handbook/people-group/learning-and-development/mentor/)


## Additional questions?

Questions, comments, and feedback can be posted in the [#mentoring](https://app.slack.com/client/T02592416/C01QKNDJ76J) slack channel. 

---
layout: handbook-page-toc
title: "Equity Compensation"
description: "At GitLab we strongly believe in employee ownership in our Company. We are in business
to create value for our shareholders and we want our employees to benefit from that shared success."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## About Your Ownership in GitLab

At GitLab we strongly believe in team member ownership in our Company. We are in business to create value for our shareholders and we want our team members to benefit from that shared success.

This guide is meant to help you understand the piece of GitLab that you’re going to own! Its goal is to be more straightforward than the full Equity Incentive Plan Documents and your equity agreement which you are advised to read, which both go into the full legal details.  Please note, however, that while we hope that this guide is helpful to understanding the equity issued to you under the Equity Plan, the governing terms and conditions are contained in the Equity Plan documents. You should consult an employment attorney and/or a tax advisor if you have any questions about navigating your equity and before you make important decisions.

## Restricted Stock Units

As of 2021-09-04, GitLab offers Restricted Stock Units (RSUs) for all future equity grants for both existing and new team members. 

### Why the change to RSUs?

* With RSUs, there is no cost to you when you vest (aside from taxes), while exercising stock options could become a significant financial burden for team members.
* RSUs as an equity vehicle offer greater stability and protection against volatility than stock options. Regardless of market fluctuations, an RSU always has value.
* RSUs are the industry standard for broad-based grants in firms past the startup stage. Stock options become less effective as a company gets larger, and the transition to RSUs is a normal step in the growth path in order to optimize our equity plans for our team members.

### RSU Vesting & Grant Cadence 

Per our [SAFE](https://about.gitlab.com/handbook/legal/safe-framework/) framework, we do not add specific dates to the handbook regarding equity. For more information on grant cadence & vesting, please see the following [slide deck](https://docs.google.com/presentation/d/1x65JGyptF3_eRQHcrESxWkBMEvbn5JVs/edit#slide=id.gf031717001_1_447). 

* RSUs begin vesting on the corresponding grant date.   
* RSUs are reviewed for approval quarterly. 
* New Hire: 12.5% vest after ~6 months (depending upon grant date - some new hires may wait a bit longer); remaining RSUs vest quarterly thereafter for 3.5 years (four-year total vest). 
* Promotion and Refresh Grants do not require a cliff, begin vesting quarterly for four-year vest. 

## FY22 Stock Option Grant Plan Design

Equity may be available to a GitLab team member as part of their overall [Total Rewards package](/handbook/total-rewards/).
The equity ranges by role are calculated as follows:
* Minimum Equity Dollar Grant:
   * Minimum of Compensation Range x Equity Percent 
* Median Equity Dollar Grant:
   * Median of Compensation Range x Equity Percent 
* Maximum Equity Dollar Grant:
   * Maximum of Compensation Range x Equity Percent 

**Definitions:**
* [Compensation Range](/handbook/total-rewards/compensation/compensation-calculator/#the-compensation-calculator-formula) is the cash compensation range as seen in the GitLab [Compensation Calculator](https://comp-calculator.gitlab.net/users/sign_in).
* [Equity Percent](https://gitlab.com/gitlab-com/people-group/peopleops-eng/compensation-calculator/-/blob/main/data/equity_calculator.yml) is a globally consistent percentage of compensation range based on the team member’s job grade.
   * The Equity Percent by grade will be updated annually in the Compensation Calculator and linked once merged.


Note: All equity grants are subject to approval by the Board of Directors and no grants are final until such approval has been obtained. The company reserves the right in its sole discretion to make any adjustments to equity grants including the decision not to make a grant at all.

## New Hire Grants

New hire grants use the formula above multiplied by a New Hire Multiplier.
The New Hire Multiplier is typically 2, but may vary.

New Hire Grants are [approved](/handbook/stock-options/#administration) by the Board of Directors or its Delegates. 

If you have any questions on what grant should be offered to a new hire, please reach out to the Total Rewards team by email at `total-rewards@gitlab.com`.

### Refresh Grants

As part of the updated [Annual Compensation Review](/handbook/total-rewards/compensation/compensation-review-cycle/#annual-compensation-review) process, eligible GitLab team members will be reviewed for a refresh grant once per year. Refresh grants use the formula above to determine the range of equity dollar value a team member may be eligible for in that cycle based on their current role/grade. Refresh grants will vest in accordance with the equity plan agreement between GitLab and the team members.

Note: All equity grants are subject to approval by the Board of Directors and no grants are final until such approval has been obtained. The company reserves the right in its sole discretion to make any adjustments to equity grants including the decision not to make a grant at all.

#### Annual Refresh Grant Program Eligibility

Team members may be eligible to participate in the annual refresh grant cycle review if they have completed six months of service with the company. However, participation will be based upon individual team member talent assessments (including if a team member is currently in good standing) and [key talent criteria](https://about.gitlab.com/handbook/people-group/talent-assessment/#key-talent-criteria). Therefore, not all team members will receive a refresh grant in the annual review cycle. Leaders will have discretion on the size of the refresh grant based on talent assessments and equity budget as allocated to each E-group member’s division. All proposed grants will be reviewed by the People team for pay equality. All proposed equity grants are subject to review and approval by the GitLab Board of Directors.

### Promotions

Promotion grants are based on the differential between the new hire equity dollar value at the new grade minus the new hire equity dollar value at the current grade using the median of the compensation range. The vesting schedule for the new equity grant will align to the terms and consitions of the equity plan for [RSUs](#restricted-stock-units). 

Formula for number of Promotion Equity Grant: `(New Hire Equity Grant Dollar Value Median at Promoted Level - New Hire Equity Grant Dollar Value Median at Previous Level)`

For example, if my current role is Backend Engineer and the median new hire equity grant dollar value grant for my role, level, and location is 20,000 USD, and I am promoted to Senior Backend Engineer and the median new hire equity grant dollar value for that role, level, and location is 40,000 USD, the calculation would be: 40,000 USD - 20,000 USD = 20,000 USD.

### FAQs

* What is a Restricted Stock Unit?
  * An RSU is simply the right to receive a share of GitLab stock at the end of the vesting period.
* Who will be granted RSUs?
  * New Hires are eligible to be granted RSUs as a new hire grant. Current team members may receive promotion or refresh grants as an RSU, if eligible.
* How will the value of my equity grant change?
  * We will continue to grant the same dollar grant value as seen in the GitLab Compensation Calculator. The number of shares will be different, but the value will be the same using RSUs as the Equity Vehicle.
* How will the amount of RSUs in a grant be calculated? What is the formula?  
  * RSUs will be converted from target equity dollar amount to shares based on the fair market value at the time of the grant.
* Will I receive fewer shares?
  * You receive the same target value of equity compensation, but since there is no exercise price on RSUs, the output as compared to options is a lower number of shares. There is no way to go “underwater” on an RSU as there is with a Stock Option. An example of this can be seen on the following [slide](https://docs.google.com/presentation/d/15uCOplWy5E9ZE45iNu2Uxwf8AymeuLmR/edit#slide=id.p4).
* What are the tax implications of RSUs?
  * Most jurisdictions tax RSUs upon vesting. The taxable amount is typically equal to the fair market value of the stock on the vest date. For specific questions, please consult your local tax advisors.
* Why do companies shift from options to RSUs?
  * [Carta](https://carta.com/blog/options-vs-rsus/?utm_source=adwords&utm_medium=paid-search&utm_campaign=13974502548&utm_term=options%20to%20rsu&utm_keyword=kwd-1391949748240&_bt=537961366920&_bk=options%20to%20rsu&_bm=p&_bn=g&_bg=127989382947&gclid=CjwKCAjwmeiIBhA6EiwA-uaeFYv3-PJRHREFxehJKZSEHR_gRFmFojZXsAFzylbGzfMRPurxJ3U9pxoCwrYQAvD_BwE) has a great article outlining many of the considerations that are taken into account when determining the right time to move from Options to RSUs.
* What will the effective date of an RSU be?
  * The effective date of an RSU (or grant date) will be the date the board approves the RSU. This is the date in which the RSU will start vesting.
* How will the vesting change with RSUs since they are taxable upon vest?
  * For more information on vesting please see the following section of the [page] (link to be added).
* Can I convert my existing options to RSUs?
  * No, all current stock options grants approved by the board will remain the same.
* I am a hiring manager, and have a new hire starting after this change. What happens to the stock options we outlined in their offer letter?
  * We will evaluate any team members with a hire date on or after Sept 4th to ensure the same value delivery as agreed to with the number of options. This value would then be converted to RSUs at the Fair Market Value at the time of the grant.
  * There is no anticipated change to the stock options outlined in the new hire’s offer letter if they start between September 1st and September 3rd.
* I am a hiring manager, how do we communicate (and when) to potential candidates about this change?
  * The Talent Acquisition team will inform all candidates of the change and alert hiring managers when candidate discussions have taken place. You also have access to the additional [talking points](https://docs.google.com/document/d/1lGFa86Sf2rcuYTZ_u4uFMAEPGmIfGqifuO2tAUyY1Wk/edit) for candidates.
* How will RSUs be withheld for tax purposes?
  * Where available, GitLab would withhold shares to pay the tax, and the team member would receive the net shares.

## Stock Options

Equity granted before 2021-09, was delivered in the form of stock options. This section is specific to those at GitLab with stock options.

At GitLab, we previously granted equity grants in the form of Incentive Stock Options (ISOs) and Non-Qualified Stock Options (NSOs). The difference in these two types of grants are, generally, as follows: ISOs were issued to US employees and carry a special form of tax treatment recognized by the US Internal Revenue Service (IRS). NSOs were granted to contractors and non-US employees. It’s called an option because you have the option to buy GitLab stock later, subject to vesting terms, at the exercise price provided at the time of grant. Solely for the purposes of example, if you are granted stock options with an exercise price of $1 per share of common stock today, and if GitLab grows later so its common stock is worth $20 per share, you will still be able to buy the common stock upon exercise of your option for $1 per share.

### Stock Option Vesting

Vesting means that you have to remain employed by, or are otherwise a service provider to, GitLab for a certain period of time before you can fully own the stock purchased under your stock option. Instead of giving you the right to purchase and own all of the common stock under your stock option on day one, you get to own the stock under your stock option in increments over time. This process is called vesting and different companies offer vesting schedules of different lengths. At GitLab, our standard practice is to issue options with a four year vesting schedule so you would own a quarter of your stock after 12 months, half of your stock after two years, and all of it after 4 years. Vesting happens on a monthly basis (so you vest 1/48 of your options each month), but many vesting schedules include a cliff. A cliff is a period at the beginning of the vesting period where your equity does not vest monthly, but instead vests at the end of the cliff period. At most companies, including GitLab, this cliff period is generally one year. This means that if you leave your job either voluntarily or involuntarily before you’ve worked for a whole year, none of your options will be vested. At the end of that year, you’ll vest the entire year’s worth (12 months) of equity all at once. This helps keep the ownership of GitLab stock to folks who have worked at the company
for a meaningful amount of time.

### Stock Splits

Companies from time to time undertake stock splits. A stock split has no economic impact on the stockholder. The split simply increases the number of shares by a certain amount and reduces the price by an equal, offsetting amount. For example, if a stockholder had 1,000 shares at $10.00 per share (total value $10,000), and the Company executed a 2 for 1 stock split, the shareholder would then have 2,000 shares at $5.00 (total value still $10,000). Nothing has changed. Public companies have historically split their stock to lower the stock price so that a broader set of investors could hold shares in the company. The theory being a lower price would make it easier for an individual investor to buy shares. Private companies perform stock splits to make themselves more comparable to other private companies. Companies that undertake IPOs typically go public in accepted trading ranges and then start trading from that point. So private companies will adjust their shares to be able to trade in those ranges at IPO. For further information, read [this article](https://smartasset.com/investing/what-is-a-stock-split).

Effective February 28, 2019 the GitLab Board of Directors approved a 4:1 stock split. All common stock, preferred stock and options were treated exactly the same in the split. The stock split will be reflected in Carta by the end of April. Why 4:1? We chose that ratio so that our shares will be comparable to other companies in our peer group, so that candidates considering coming to work at GitLab can make an informed choice on an “apples to apples” basis. We also took care to not split at too high of a ratio which could result in a [reverse stock split](https://www.bizjournals.com/sanjose/stories/2001/01/15/focus3.html) prior to IPO and that is something we would like to avoid (but can't guarantee in any scenario).

### Exercising Your Options

"Exercising your options" means buying the stock guaranteed by your options. You pay
the exercise price that was set when the options were first granted and you get stock
certificates back.
To give employees an opportunity to benefit from any existing tax incentives that may
be available (including under the US and the Dutch tax laws) we have made the stock
immediately exercisable. This means you can exercise your right to purchase the unvested
shares under your option to start your holding period. However, the Company retains a
repurchase right for the unvested shares if your employment or other services ends for
any reason. An early exercise of unvested stock may have important tax implications and
you should consult your tax advisor before making such decision.

Also, while the company has the right to repurchase the unvested shares upon your
termination of services, the company is not obligated to do so. Accordingly you could
lose some or all of the investment you made. Because we are a young company there are
lots of risks, so be aware and informed of the risks. Please read this [quora thread about most startups failing](https://www.quora.com/What-is-the-truth-behind-9-out-of-10-startups-fail) and this story of [people paying more in tax for their stock than they get back](http://www.nytimes.com/2015/12/27/technology/when-a-unicorn-start-up-stumbles-its-employees-get-hurt.html).

#### Trading Restrictions During 409a Valuations

GitLab team members can access the options exercise restriction periods schedule by visiting [this link](https://drive.google.com/file/d/10ECV9zA56zLARuLqhXj6QTNqKOUx-RwC/view?usp=sharing).

#### Option Expiration

If you leave the company, you will generally have [90 days to exercise your option](#exercise-window-after-termination)
for any shares that are vested (as of the last day of service).

In addition, if not otherwise expired (through termination of your employment), your stock
options expire 10 years after they were issued.

#### Exercise Window After Termination

Please note that until the [post IPO lockup period](http://www.investopedia.com/ask/answer/12/ipo-lockup-period.asp) has expired (or we are bought) company stock is not liquid. If your employment ends
for whatever reason, you have a 90 day window to exercise your vested options, or lose them. During this window you would have to pay
the exercise price and in some cases the tax on the gain in value of your stock options, which could be considerable.
If the company stock is not liquid this money might be hard to come by. The 90 day window is an industry standard but
there are [good arguments against it](https://zachholman.com/posts/fuck-your-90-day-exercise-window/).
You may not purchase unvested shares after your service has ended.

At GitLab the stock options are intended to commit our team members to get us to
a successful public listing of our stock. We want to motivate and reward our people for reaching that goal. Therefore we will consider exercise
window extensions only on a case by case basis at our discretion. An example of a situation we'll consider is a valued
team member quitting because of personal circumstances. In most cases there will be no extension and you will either have
to pay for shares and the taxes yourself, or lose the options, even when you are fully vested. And, of course, [being a publicly listed company is our public ambition](/company/strategy/),
but neither the timing, nor whether it happens at all, is guaranteed.

If you leave GitLab after less than a year, your Carta account will stay active but all unvested GitLab shares (which will be all of them as our vesting schedule starts at 1 year) will show as expired and you will not be able to take any actions with those shares. At the exercise expiration date, since no options were exercised, all options will return to the option pool to be reissued by GitLab.

You can reach a Carta support rep by calling (650) 669-8381 but you will have to enter a support PIN that is associated with you. You can find this PIN by logging into your Carta account, clicking Help, then Contact Us and the PIN will be there.

#### How to Exercise Your Stock Options

There are two methods to exercise your shares:

1. Electronic (US Residents Only)
  - Log into your Carta account
  - Follow the directions to enable ACH payments from your bank
  - After ACH has been enabled select exercise option grants and follow the prompts
2. Manual (Non ACH and Non US Residents)
  - Log into your Carta account
  - On Portfolio Details, click the "Holdings" option
  - On the option grant list, click the arrow down icon on right side of the screen and select the "View" option
  - A modal will open. On the left side menu, click "Documents and notes"
  - Download the "Form of Exercise Agreement" file
  - Complete the form, sign, and return as PDF to `stockadmin@gitlab.com`
  - Send payment in US dollars by wire transfer. You will be provided the wire transfer info.

**Note for [US Persons for tax purposes](https://www.irs.gov/individuals/international-taxpayers/classification-of-taxpayers-for-us-tax-purposes):** whichever method you choose, if you **early exercise** unvested shares, be sure to download the **83B election form** provided by Carta and file with the IRS within 30 days of exercise. Send a copy of the signed and dated election form to `stockadmin@gitlab.com`. The Stock Plan Administrator will file it within Bamboo/HR.

See Carta's Support article [83(b) Form for Early Exercised Option Grants](https://support.carta.com/s/article/form-83b-option-exercises) -- [download the 83-b election form template here](https://carta.my.salesforce.com/sfc/p/#f4000000nrKS/a/0H000000Xm1Z/H0r8oAMKO54MeOPha6Y2ej8zlCnIrPzVyQsFP5XdboU).

You will most likely want to include the following letter when sending in the 83-b election to the IRS:

```
<<Date Filed>>

Department of the Treasury

<<Address provided from Carta 83(b) instructions>>

To whom it may concern:

Please find enclosed two copies of the 83(b) election in connection with my purchase of shares of GitLab Inc. common stock. Please return one copy stamped as received to my attention in the enclosed self addressed stamped envelope.

Yours Truly,

//signature
```

### Exercise Prices and 409A Valuations

Generally, the exercise price for options granted under the 2015 Equity Plan will be
at the fair market value of such common stock at the date of grant. In short, “fair
market value” is the price that a reasonable person could be expected to pay for the
common stock, but because GitLab is not “public” (listed on a large stock exchange),
the Board is responsible for determining the fair market value. In order to assist
the Board, the company retains outside advisors to undertake something called a “409A
valuation”. In general, the lower a valuation for the shares
the better for employees as there is more opportunity for gain. Additionally, a lower
exercise price reduces the cash required to exercise the shares and establish a holding
period which can have tax advantages in some countries. We describe those in this document
but as always check with your financial or tax advisor before taking any action.

## Administration

GitLab is currently migrating to a new Administration Tool. The Stock Admin team will update this information as available. 

## Taxes

Tax law is complex and you should consult a tax attorney or other tax advisor who is
familiar with startup stock options before making any decisions. Please go to the [tax team's stock options](/handbook/tax/stock-options/) page for more information about the taxation of stock options.

### US employees with Incentive Stock Options (ISOs)

Taxation from the US perspective is not as straightforward as you might like. You aren’t taxed when you exercise
your options. Tax is due on the gain or profit you make when you sell the stock. Depending on your holding period,
the tax may be treated as ordinary income or capital gain. Moreover, when you hold the options long enough you may be subject to 0% capital gains tax. To outline the five possibilities of the different scenarios that may apply:

1. Exercise your options to purchase shares, and hold.
1. Exercise your options to purchase shares, and sell the shares within the same year.
1. Exercise your options to purchase shares, and sell the shares in less than twelve months, but during the following year.
1. Sell shares at least one year and a day after you exercised the options, but less than two years from the original grant date.
1. Sell shares at least two years from the original grant date.

Please note, however, that any gain upon exercise of an ISO (difference between the exercise price and fair market value
at date of exercise), even if you do not sell the shares, may be counted as a "tax preference"
towards the Alternative Minimum Tax limit. For instance, under scenario 1 above you have to make an adjustment in your tax return for the Alternative Minimum Tax (AMT) that equals the so-called bargain element. Each scenario has a different tax treatment, so be careful of the tax consequences when you exercise your options. In the long term, holding onto your stock does save taxes, however be aware of the AMT that you will be confronted with. It is strongly advised that you contact a tax advisor to be aware of the US tax consequences.

In addition to the benefits of a longer holding period, the IRS does have an additional benefit for holders of Qualified Small Business Stock (QSBS for short). GitLab meets the criteria for QSBS treatment for options exercised prior to August, 2018, however (again), the Company is not in a position to offer tax or legal advice nor does it make any representation about compliance with the QSBS provisions, so check with your own tax and financial advisors. We found [this article](https://blog.wealthfront.com/qualified-small-business-stock-2016/) helpful in describing the QSBS program in greater detail.

### US service providers with Nonqualified Statutory Options (NSOs)

For non-employees of GitLab that have been granted stock options, their stock options are treated as NSOs. NSOs have different tax treatments depending on whether they are actively traded on an established market:

- **when actively traded**: taxed at grant
- **when not actively traded**: taxed at exercise

Since GitLab's stock options are not actively traded on an established market, the NSO is taxed at exercise. The gain of the exercise (fair market value minus the exercise price) has to be reported by [form 1099-MISC (box 7)](https://www.irs.gov/pub/irs-pdf/f1099msc.pdf). Withholding is typically not required, however when the service provider fails to provide a valued tax identification number in form 1099, GitLab has to ensure backup withholding (roughly 25%).

### Netherlands and Germany

For our employees based in the Netherlands and Germany, the difference between the exercise price and the fair market value is considered taxable at the date you exercise your stock options. With respect to reporting taxes: the taxable gains are subject to employer tax withholding. The tax payable is therefore deducted from your gross payroll with respect to the exercise of your stock options.

### The United Kingdom

In the United Kingdom there is a small difference in the tax treatment of exercising your stock options as opposed to the other entities; the difference between the fair market value and the exercise price is taxed through payroll at the date of exercise if there is a liquid market for the stock at time of exercise. If there is no liquid market, income tax will likely still be due but the team member will need to settle this directly with HMRC. For more information please check [UK's stock option page](/handbook/tax/stock-options/unitedkingdom/).

## Dual-Class Stock

Often companies will create multiple classes of stock, with each class having different voting rights, in order to provide protection to the company's founders, early investors, and early employees, whose long-term vision for the company may not align with that of later stage investors. At the GitLab Board of Directors meeting held on January 31, 2019, the Board approved the creation of such a dual-class structure.

### Effect of Dual-Class Stock

- Class B stockholders retain significant influence over stockholder votes and actions.
- The Influence of long-term Class B stockholders increases as other stockholders from before being public sell or distribute their Class B stock, which coverts into Class A common stock upon sale or distribution.

### Dual-Class Voting

| Type                     | Detail             |
| :----------------------- | :----------------- |
| **Class A Common Stock** | 1 vote per share   |
| **Class B Common Stock** | 10 votes per share |

### Creation of Dual-Class Stock

**Step 1: Before being a Publicly Traded Company**

- Create two classes of common stock.
- Outstanding common stock converts to Class B common stock.
- Options and RSUs subsequently issued exercise into Class A common stock.

**Step 2: When Going Public**

- Preferred stock converts to Class B common stock.
- Class A common stock sold in public offering.

**Step 3: Being Public**

- Class B converts to Class A in sale or distribution unless “permitted transfer” (see below).
- Additional sales of Class A common stock.
- Permitted transfers would be transfers to one or more family members, transfers to a trust for the benefit of the stockholder or in favor of a family member of the stockholder, or transfers to a general partnership, limited partnership, limited liability corporation, or other entity controlled by the stock holder or a family member of the stockholder.

### Sunset

The stock structure will automatically convert and become a single-class structure upon (whichever comes first):

- A date specified by a vote of 66.66% of the outstanding shares of Class B common stock
- Death or Permanent Disability of Sid Sijbrandij
- At such time as when the Class B represents less than 5% of the outstanding Common Stock

## Transfer Restrictions on Common Stock

On January 31, 2019 the Board of Directors approved the amendment to the company's bylaws regarding the transfer of shares of Common Stock. Effective as of that date, Stockholders will not be able transfer, sell, or assign any shares of Common Stock without the prior written consent of the Board. This restriction does not apply to the following permitted transfers:

- the transfer is a gift or pursuant to a domestic relations order, to the stockholder's immediate family member;
- the transfer is executed pursuant to the stockholder's will or the laws of intestate succession;
- the transfer by an entity stockholder is made to an affiliate of such entity stockholder;
- the transfer by an entity stockholder of all Common Shares is made to a single transferee in accordance with the terms of any bona fide merger, consolidation, or acquisition;
- the transfer is made for no consideration by a stockholder that is a partnership to such stockholder's limited partners in accordance with the partnership interests of such limited partners;
- any repurchase or redemption of the Common Shares by the corporation (a) at or below cost, upon the occurrence of certain events, such as the termination of employment or services, or (b) at any price pursuant to the corporation's exercise of a right of first refusal to repurchase such Common Shares; or
- in the event of a transfer or deemed-transfer that is approved, or in the event the restriction is waived, and the Common Shares of the transferring stockholder are subject to co-sale rights, any transfers by the persons and/or entities who are entitled to and have exercised the co-sale rights in conjunction with such approved transfer or deemed-transfer giving rise to the exercise of such co-sale right.

## Questions?

Everyone is always welcome to ask our CFO any questions they have about their options,
GitLab’s fundraising, or anything else related to equity at GitLab. However, everyone should
also consult a lawyer before making important financial decisions, especially regarding
their equity because there are complex legal and tax requirements that may apply.

### References

Our team member Drew Blessing [wrote about what he learned about stock options](http://blessing.io/startups/stock-options/2016/02/15/navigating-your-stock-options.html) after starting to research them because he received them when joining us. His article is greatly appreciated, but any advice is his own, and it is not officially endorsed by GitLab Inc.

## Procedures for Issuing Options to Team Members

1. In Carta Download the Bulk Import Sheet.
1. Cut and paste the Option Grant sheet approved by the Board into the Bulk Import Sheet.
1. Use the concatenate function to merge team member names into a single cell.
1. Designate options as ISO, NSO or INTL. Carta will automatically convert ISOs over the limit into NSOs.
1. Issue date relation - Enter all team members, advisors and board members as Employees. GitLab early adopted ASU 2018-07 (w.r.t accounting for stock options issued to non-employees) which is why we designate all team members as employees.
1. The standard vesting schedule is 1/48 monthly with a 1 year cliff.
1. Enter YES for Early Exercise.
1. Check with Fenwick on Federal and State Exemption whether Rules 701 and 21052(f) remain applicable.
1. Ensure any non-standard vesting terms are correctly input on bulk import or individually in Carta.
1. Document Set is "2015 Equity Incentive Plan".
1. All other optional fields can be left blank as default settings have been pre-populated in Carta.
1. When successfully uploaded make announcement in Team Call. All hires and promotions through xx date; all refresh grants for hires through yy date.

## Procedures for Adding Team Members Department/Cost Center in Carta

1. Once the new awards have been loaded and approved, the team members’ department needs to be added manually to Carta.  Note: This is only being done for team members, not Investors.
1. The headcount report is downloaded from Bamboo for all team members.
1. In Carta, choose “Stakeholders - All Stakeholders”
1. Sort on the “Cost Center” to move those with an empty field to the top of the group
1. Click the down arrow to the far right of the team member record/row, and choose “Edit Stakeholder Properties”
1. In the new window, make sure you’re in the “Cost Center” and enter the effective date (using last grant date) and the cost center according to the headcount report, in exact form - and save.
1. Make sure this is done for all team members that have a blank cost center field.

## Procedures for Terminating a Team Member in Carta with share repurchase

1. In Carta, go to -> Stakeholders -> All Stakeholders -> and search for team member
2. Far right of the line, in the drop down choose “Terminate Stakeholder”
3. Begin entering termination information
- Termination Date
- Termination Type = Voluntary or Involuntary
- New Relationship = Ex-employee
- Notifications - Add a new email, which is the personal email address and can be found under their profile
4. For shares being repurchased:  Confirm the dates to exercise and repurchase are correct and check “I confirm the last day to repurchase is correct for each certificate” and save the termination
5. To repurchase a certificate, navigate to 'Shares' under Securities. Then select 'Repurchase shares' in the drop-down menu for the appropriate tranche.
6. Enter the certificate repurchase information (date, number of shares, transaction value) and make sure the “Transfer funds via ACH” toggle is on. Next: review repurchase details
7. Review the details of the repurchase (shares, value, date) and confirm repurchase
8. The final screen will confirm the total funds that will be withdrawn from the company account and transferred to the team member for the repurchase of the shares.
9. Send email to team member bcc'ing the CFO and Peopleops.  A sample email is below:
- “Hello,[team member], just to confirm that GitLab has initiated a repurchase of your unvested shares.  The transaction has been processed through Carta.  You should expect to see an ACH transaction into your bank account in the amount(s) of $xxx.xx (and $x,xxx.xx, if necessary).  Please contact me if you have any concerns or questions.”
10. Send to their personal email address
11. File email in GitLab Inc=> Equity => Repurchases
12. Send note to Accounting and External Reporting, and get screenshot of transfer out of Comerica
13. File screen shot as above.
14. Once all steps are completed, ensure the offboarding issue is marked as such by checking the box for Carta, assigned to the Stock Plan Administrator

## Procedures for Processing a Manual Exercise

1. After the company has received the signed exercise agreement and the payment from the team member, the Stock Plan Administrator will compare the form and calculate the exercise costs to ensure the funds received are accurate.
2. The Stock Plan Administrator will request a screenshot of the payment received and save it to the “Equity Cash Receipts - Temporary” folder.
3. The exercise form will be sent to the CFO via DocuSign with request for signature.  Once signed, the exercise form and cash receipt will be downloaded to the “Equity - Option Exercise Agreements” folder.
4. Once the documents are ready to load, the Stock Plan Administrator will process the exercise in Carta.
5. The signed exercise form will then be uploaded to the team members Carta account for record keeping.
6. Navigate to the appropriate option grant in Carta and select 'Exercise options' in the drop-down menu.
7. Fill out all pertinent details about the exercise date and the number of options to be exercised. Then click 'Next: review certificate details.'
8. The newly issued certificate now shows up under the common certificate section of the cap table.

## Stock Administrator Performance Indicators

### Number of award transactions processed

The number of award transactions processed in Carta over a quarter. This is measured on the last calendar day of the quarter. The target has not been determined.

### Number of participants supported

The number of participants supported is measured on the last day of the calendar month.

## Equity Knowledge Assessment

The Equity Knowledge Assessment is currently being updated to reflect RSU updates.

---
layout: handbook-page-toc
title: "Brand Resources"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Brand Resources
{:.no_toc}

### Overview
Our brand is the embodiment of our [mission](https://about.gitlab.com/company/mission/#mission), [vision](https://about.gitlab.com/direction/#vision), and [values](https://about.gitlab.com/handbook/values/). As stewards of the [GitLab brand](/company/brand/), our goal is to educate and enable the wider organization with resources to effectively and honestly communicate what the company does for our internal and external audiences.
### Brand Standards
The specifications below detail how our various design elements work together to express a consistent visual brand identity. The standards serve as a resource for all corporate marketing materials and outline best practices for creating on-brand assets.

Last updated: 2021-06-30
<html>
  <head>
    <title>PDF Viewer</title>
  </head>
  <body>
    <div>
      <object
        data='/resources/gitlab-brand/gitlab-brand-standards.pdf'
        type="application/pdf"
        width="820"
        height="650"
      >

        <iframe
          src='/resources/gitlab-brand/gitlab-brand-standards.pdf'
          width="820"
          height="650"
        >
        <p>This browser does not support PDF!</p>
        </iframe>

      </object>
    </div>

  </body>
</html>

The above standards should be applied to all marketing materials, including, but not limited to: digital ads, events, publications, merchandise, etc. For additional design considerations, refer to the following:

- GitLab product: [Pajamas](https://design.gitlab.com/) design system
- GitLab website: [Slippers](https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/slippers-design-system/) design system 

### Tone of voice

The following deck outlines the set of standards used for all written company communications to ensure consistency in voice, style, and personality across all of GitLab's public communications.

<figure class="video_container"><iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQnRSaLsmAyGxcDHj3a_uh5UT2h45WUKqF0UGwtedOVGUK0iCcC134czSZ_Hv7cOvLF9BpKM0_frZMS/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe></figure>

Additional resources:

- Video walk-through: [tone of voice](https://youtu.be/j-viaR6qhXM)
- Style guide: [blog/editorial](https://about.gitlab.com/handbook/marketing/inbound-marketing/content/editorial-team/#blog-style-guide)
- Style guide: [writing (general)](https://about.gitlab.com/handbook/communication/#writing-style-guidelines)

### Brand oversight

Occasionally the [old GitLab logo](https://gitlab.com/gitlab-com/gitlab-artwork/blob/master/_archive/logo/fox.png) is still in use on partner websites, diagrams or images, and within our own documentation. If you come across our old logo in use, please bring it to our attention by creating an issue in the [Marketing](https://gitlab.com/gitlab-com/marketing/general/issues) issue tracker. Please include a link and screenshot (if possible) in the description of the issue and we will follow-up to get it updated. Thanks for contributing to our brand integrity!

### Templates

#### Presentation kits
- [General GitLab deck template](https://docs.google.com/a/gitlab.com/presentation/d/16FZd01-zCj_1jApDQI7TzQMFnW2EGwtQoRuX82dkOVs/edit?usp=sharing)
- [GitLab pitch deck template](https://docs.google.com/a/gitlab.com/presentation/d/1LC1lT-gxpl1oUZ2InX4Oni9T4MfR0DFF0RLi4uNxBBQ/edit?usp=sharing)




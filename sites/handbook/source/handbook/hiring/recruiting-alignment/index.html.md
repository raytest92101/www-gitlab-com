---
layout: markdown_page
title: "Talent Acquisition Alignment"
description: "This page is an overview of the search team alignment and the talent acquisition platform directly responsible individual in talent acquisition operations and talent brand."
---

## Search Team Alignment by Department

| Department                    | Recruiter       | Sourcer     | Candidate Experience Specialist    |
|--------------------------|-----------------|-----------------|-------------------------------------|
| Executive          | Rich Kahn   | Chriz Cruz | Ale Ayala |
| Enterprise Sales, NA | Kevin Rodrigues |  N/A | Hannah Stewart |
| Enterprise Sales, EMEA | Debbie Harris & Kanwal Matharu |  TBH | Lerato Thipe |
| Enterprise Sales, APAC | Debbie Harris |  TBH | Lerato Thipe |
| Commercial Sales,	AMER | Marcus Carter & Shannan Farmer | TBH  | Hannah Stewart |
| Commercial Sales,	EMEA/APAC | Ben Cowdry | TBH | Lerato Thipe |
| Channel Sales, Global | Debbie Harris & Kanwal Matharu | TBH | Lerato Thipe |
| Field Operations,	Global | Kelsey Hart | Loredana Iluca | Hannah Stewart |
| Customer Success, EMEA | Joanna Muttiah | Loredana Iluca | Lerato Thipe |
| Customer Success, APAC | Joanna Muttiah | Loredana Iluca | Lerato Thipe |
| Customer Success, NA | Barbara Dinoff | Loredana Iluca | Hannah Stewart |
| Marketing, Global | Steph Sarff   | Alina Moise | Davie Soomalelagi |
| Marketing, SDR Global | Tony Tsiras | Tony Tsiras| Davie Soomalelagi |
| G&A, Finance, People, CEO | Maria Gore | Alina Moise | Davie Soomalelagi |
| G&A, Accounting, Legal | Rachelle Druffel | Alina Moise | Davie Soomalelagi |
| Development | Mark Deubel, Riley Smith, Sara Currie | Zsuzsanna Kovacs and Susan Hill | Michelle Jubrey (temporary) |
| Quality | Rupert Douglas   | Zsuzsanna Kovacs | Davie Soomalelagi (temporary) |
| UX  | Rupert Douglas   | Zsuzsanna Kovacs  | Davie Soomalelagi (temporary) |
| Support | Joanna Michniewicz  |  Joanna Michniewicz | Michelle Jubrey |
| Security | Nicky Kunstman |  Zsuzsanna Kovacs | Michelle Jubrey |
| Incubation | Rupert Douglas  |  Zsuzsanna Kovacs | Davie Soomalelagi (temporary) |
| Infrastructure   | Josh Barker  | Susan Hill | Michelle Jubrey |
| Product Management  | Matt Allen | Chris Cruz | Michelle Jubrey |

For urgent requests of the Candidate Experience Specialist team, we encourage you to contact them by also tagging @CES in Slack messages and CC'ing CES@gitlab.com on emails. 

## Talent Acquisition Leader Alignment

| Department                    | Leader      | 
|--------------------------|-----------------|
| Talent Acquisition         | Rob Allen |
| Talent Brand and Enablement | Devin Rogozinski |
| Talent Acquisition (Sales) | Jake Foster|
| Talent Acquisition (Marketing) | Kelly Murdock |
| Talent Acquisition (G&A) | Jake Foster |
| Talent Acquisition (R&D) | Ursela Knezevic |
| Candidate Experience | Marissa Feber |
| Strategic Programs | Kelly Murdock |
| Inbound Sourcing | Chris Cruz |

## Talent Acquisition Platform Directly Responsible Individual

| Platform                    | Responsibility        | DRI     |
|--------------------------|-----------------|-----------------|
| Comparably | Admin  | Devin Rogozinski/Marissa Ferber |
| Comparably | Content Management | Devin Rogozinski |
| Comparably | Reporting | Marissa Ferber |
| Glassdoor | Admin  | Devin Rogozinski |
| Glassdoor | Responding to Reviews  | Devin Rogozinski |
| Glassdoor | Content Management | Devin Rogozinski |
| Glassdoor | Reporting | Marissa Ferber |
| LinkedIn | Admin - Recruiter  | Marissa Ferber |
| LinkedIn | Seats | Marissa Ferber |
| LinkedIn | Media - General | Marketing |
| LinkedIn | Media - Talent Acquisition | Devin Rogozinski |
| LinkedIn | Content Management | Marketing |
| LinkedIn | Content Management - Life at GitLab | Devin Rogozinski |
| New Platform(s) | Requests | @domain |
| Recruitment Marketing  | Requests | @domain |

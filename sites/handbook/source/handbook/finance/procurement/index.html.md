---
layout: handbook-page-toc
title: The GitLab Procurement Team
---

{::options parse_block_html="true" /}

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc}

- TOC
{:toc}


## <i class="far fa-paper-plane" id="biz-tech-icons"></i> What is Procurement?
Procurement is the process of selecting vendors, strategic vetting, selection, negotiation of contracts, and the actual purchasing of goods. Procurement acquires all the goods, services, and work that is vital to GitLab.

## <i class="far fa-paper-plane" id="biz-tech-icons"></i> Procurement Mission and Policy
- The mission of the Procurement Team at GitLab is to be a strategic business partner for all departments to drive value through external partnerships.
- Team members can purchase goods and services on behalf of the company in accordance with the [Signature Authorization Matrix](/handbook/finance/authorization-matrix/) and guide to [Spending Company Money](/handbook/spending-company-money/). 
- All purchases made on behalf of GitLab that do not qualify as a personal expense, need an approved Purchase Order (PO) in Coupa. 

**Exceptions to the PO Policy are:**
1. Purchases under $5K
1. Charitable Contributions (Donations)
1. Computer/Hardware Advances (if unable to be paid through Payroll Dept)
1. Interview Candidate Reimbursement
1. Legal Fees
1. Audit and Tax Fees
1. Benefits, PEO Providers and Payroll
1. AR/Customer Refunds
1. Board of Director Payments
1. Financing, Banking and Investing (incl interest, debt, FX, fees)
1. Corporate Credit Card
1. Urgent Payments not included on list above (approval required from VP, Corporate Controller and/or PAO)

## <i class="fas fa-stream" id="biz-tech-icons"></i> What are you buying?

Choose the type of purchase you're making for the process guidance:
<div class="flex-row" markdown="0" style="height:110px;justify-content:center">
  <a href="/handbook/finance/procurement/new-software/" class="btn btn-purple" style="white-space: initial;min-width: 0;width: auto;margin:5px;display:grid;align-items:center;height:100%;">New Software</a>
  <a href="/handbook/finance/procurement/software-renewal/" class="btn btn-purple" style="white-space: initial;min-width: 0;width: auto;margin:5px;display:grid;align-items:center;height:100%;">Software Renewal or add-on</a>
  <a href="/handbook/finance/procurement/professional-services/" class="btn btn-purple" style="white-space: initial;min-width: 0;width: auto;margin:5px;display:grid;align-items:center;height:100%;">Professional Services / Contractors / Agencies</a>
  <a href="/handbook/finance/procurement/field-marketing-events" class="btn btn-purple" style="white-space: initial;min-width: 0;width: auto;margin:5px;display:grid;align-items:center;height:100%;">Field Marketing and Events</a>
  <a href="/handbook/finance/procurement/office-equipment-supplies/" class="btn btn-purple" style="white-space: initial;min-width: 0;width: auto;margin:5px;display:grid;align-items:center;height:100%;">Home office equipment and supplies</a>
  <a href="/handbook/finance/procurement/personal-use-software/" class="btn btn-purple" style="white-space: initial;min-width: 0;width: auto;margin:5px;display:grid;align-items:center;height:100%;">Individual Use Software</a>
</div> 

## <i class="fas fa-stream" id="biz-tech-icons"></i> Toolkits
Includes RFP Templates, scorecards and Vendor Guidelines for doing business with GitLab:

<div class="flex-row" markdown="0" style="height:110px;justify-content:center">
   <a href="/handbook/finance/procurement/vendor-selection-process/" class="btn btn-purple" style="white-space: initial;min-width: 0;width: auto;margin:5px;display:grid;align-items:center;height:100%;">Vendor selection process</a>  
   <a href="/handbook/finance/procurement/vendor-guidelines/" class="btn btn-purple" style="white-space: initial;min-width: 0;width: auto;margin:5px;display:grid;align-items:center;height:100%;">Vendor guidelines</a>
   <a href="/handbook/legal/NDA/#" class="btn btn-purple" style="white-space: initial;min-width: 0;width: auto;margin:5px;display:grid;align-items:center;height:100%;">Non-Disclosure Agreement (NDA)</a>
   <a href="/handbook/finance/procurement/charitable-contributions/" class="btn btn-purple" style="white-space: initial;min-width: 0;width: auto;margin:5px;display:grid;align-items:center;height:100%;">Charitable Contributions</a>
   <a href="/handbook/finance/procurement/trial-agreement/" class="btn btn-purple" style="white-space: initial;min-width: 0;width: auto;margin:5px;display:grid;align-items:center;height:100%;">Demo/Trail Agreements</a>
</div>

## <i class="fas fa-stream" id="biz-tech-icons"></i> How do I contact Procurement?
- If you have a question, use the #procurement slack channel.

## <i class="fas fa-stream" id="biz-tech-icons"></i> How do I access Coupa to create a Purchase Request?

Coupa is available via Okta. To access the platform:
1. Login to your [Okta home page](https://gitlab.okta.com/app/UserHome#).
1. Click on the Coupa (Prod) button.
   - A new tab should open with your user logged in.

Every month all Coupa access will be reviewed and users who haven't been active in a period of 90 days will have their access removed.
If you need to request access again, please reopen your initial Access Request issue and tag the Finance Systems Admins team using `@gitlab-com/business-technology/enterprise-apps/financeops` in a comment.
{: .alert .alert-warning}

If your job function requires you to submit purchase requests in Coupa, follow the below steps:
1. Open an [Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new) for Coupa using the `Individual_Bulk_Access_Request` template.
1. In Step 2, in the _Justification for this access_ question, please describe:
   - What you need to buy.
   - What is the total cost of the purchase. 
   - How often you will need to purchase it.
   - When does it need to be submitted in Coupa.
1. Add the labels ~"FinSys - Coupa" and ~"FinSys::Service Desk".

<div class="panel panel-success">
**Best Practices**
{: .panel-heading}
<div class="panel-body">
Due to the limited number of licenses available for Coupa, it is recommended that each department identify power users responsible for creating purchase requests on the team’s behalf.

</div>
</div>


## <i class="fas fa-book" id="biz-tech-icons"></i> Related Docs and Templates

##### Contract Templates

- [GitLab Vendor Terms and Conditions](https://about.gitlab.com/handbook/finance/procurement/vendor-guidelines/)
- [Logo Authorization Template](https://drive.google.com/file/d/1Vtq3UHc8lMfIbVFJ3Mc-PZZjb6_CKAvm/view?usp=sharing)
- [Data Processing Addendum](https://docs.google.com/document/d/188OI1xC63VpqWNbot-O-KmUXPAqh7q0s/edit?usp=sharing&ouid=105676530718857820161&rtpof=true&sd=true)
- [EU Standard Contractual Clauses](https://drive.google.com/file/d/1oOgETU0YZrp_JhM88oI57DsLEh1qpvFV/view?usp=sharing)
- [US Contractor Agreement](https://docs.google.com/document/d/1A8mnzoJ_lJ3MP-ZUTm29n1Xev4H6EQyG/edit?usp=sharing&ouid=101595031187349560306&rtpof=true&sd=true)
- [SOW Template](https://docs.google.com/document/d/1IUgGGxmNSPZ3gJK-qt2LZ1vsKJZG1oWs/edit?usp=sharing&ouid=101595031187349560306&rtpof=true&sd=true)
- [Change Request Template](https://docs.google.com/document/d/1AHagOA8g_RSt9cpVHrlxiRHKOutcxvJ6/edit?usp=sharing&ouid=101595031187349560306&rtpof=true&sd=true)

#### Documentation

* [Non-Disclosure Agreement (NDA) Process](https://about.gitlab.com/handbook/legal/NDA/)
* [Uploading Third Party Contracts to ContractWorks](/handbook/legal/vendor-contract-filing-process/)
* [Company Information](https://gitlab.com/gitlab-com/Finance-Division/finance/-/wikis/company-information) - general information about each legal entity of the company
* [Trademark](/handbook/marketing/corporate-marketing/brand-activation/brand-standards/#trademark) - information regarding the usage of GitLab's trademark
* [Authorization Matrix](/handbook/finance/authorization-matrix/) - the authority matrix for spending and binding the company and the process for signing legal documents

{::options parse_block_html="false" /}

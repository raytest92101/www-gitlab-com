---
layout: handbook-page-toc
title: "Remote.com"
description: "Discover the benefits for Remote team members"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

The following benefits are provided by [Remote](https://www.remote.com/) and apply to team members who are contracted through Remote. If there are any questions, these should be directed to the Total Rewards team at GitLab who will then contact the appropriate individual at Remote.

## Denmark

All of the benefits listed below are administered and managed by [Remote](https://remote.com/country-explorer/denmark)

### Medical

GitLab does not plan to offer Private Health Insurance at this time because team members in Denmark can access the [public healthcare system](https://www.healthcaredenmark.dk/). This healthcare system covers all the team members and their family members.

### Pension

Remote will directly contribute to a Group Pension Plan (Soderberg & Partners). The team member will contribute 4% of the monthly base salary, with the Employer contributing 8% of the team members monthly base salary. The contribution to the pension savings plan will be paid directly into the pension plan in conjunction with the Employer’s payment of the monthly salary to the team member.


### Life Insurance

GitLab does not plan to offer life insurance at this time as team members can access the benefits from Social insurance system if they get ill, injured or have a disability. The [GitLab Life Insurance plan](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#life-insurance) still applies.



### Remote - Denmark Statutory Leave Policies

#### Holiday Leave

All full-time workers are entitled to 5 weeks days paid holiday leave a year under the Holiday Act (Lov om ferie). Holidays accrue from the previous calendar year and can be taken in the holiday year which runs from 1 May to 30 April in the following year.

#### Pregnancy, Maternity and Paternity leave

In total, parents in Denmark get 52 weeks of paid parental leave. The general rule is that the mother has the right to four weeks of leave directly before the planned birth and then to a further 14 weeks of leave after birth.

The father or co-mother is entitled to take two weeks of leave during the first fourteen weeks after the birth of the child. Then 32 weeks follow where the mother and father can freely share leave between them. They can choose to be on parental leave at the same time or in periods one after the other. 

#### Maternity Leave Payment
As an employee, you can receive maternity benefits if you meet these three conditions:

* You must be employed on the first day of your leave or the day before.
* You must have at least 160 hours within the last four full months before the leave.
* You must have at least 40 hours per month for at least three of the four months.
* In addition, you must also be with your child daily, ie. physically spend time with the child.

**Payment Denmark automatically receives the information about your employment and hours from your employer.**

During maternity leave (after childbirth), birthing parents will receive 50% of their salary from the State. However, employer and employee can agree to full pay entitlement. 

The allowance during parental leave is calculated as an amount per week. The amount depends on how much you work. The maximum amount per week is DKK 4.460 (2021). 

* If you are [eligible](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave), you will receive 100% paid Parental Leave for up to 16 weeks. Any additional leave time taken (up to 52 weeks total used between both parents) will be paid at 50% from the State.

*Remote will seek reimbursement from the public authority; Udbetaling Danmark.

#### Applying for Maternity Leave Pay

**If you receive pay during the leave**
If you receive pay during the leave, your employer will be paid the maternity allowance in the meantime. It's called reimbursement. You can only apply for maternity benefit yourself when the salary stops.

Your employer must notify Udbetaling Danmark when the salary stops. They can do this at the earliest on your first day without pay. As soon as your employer has given notice, you will automatically receive a letter requesting maternity benefits.

They must also give notice if part of your salary stops. In some cases, you can then receive an amount in maternity benefit in addition to the salary.

If you have not received the letter shortly after the salary stops, you must contact your employer to start applying. You must have applied, no later than 8 weeks after all or part of your salary has stopped.

If you do not know what day your pay will stop, ask your employer. Payment Denmark only knows after your salary has stopped and your employer has given notice.

**If you do not receive pay during the leave**
If you do not receive pay during the leave, you must apply for maternity benefit yourself from the start of the leave.

Your employer or unemployment insurance fund must first notify Udbetaling Danmark that you are going on leave. They can do this your first day on leave at the earliest.

As soon as they have given notice, you will receive a letter with a link to apply for maternity allowance. It also says when you last applied.

If you have not received the letter shortly after you have gone on leave, you must contact your employer or unemployment insurance fund to start applying.

To help you plan for your leave, click on [Calculating your Maternity Leave](https://www.borger.dk/Handlingsside?selfserviceId=9afd00f4-efd1-448f-94b9-6d66fd95d505&referringPageId=a2a65391-2201-4d47-ad25-2b0e0a343fad&type=DK).

#### Paternity Leave 
Fathers are entitled to 2 weeks of paternity leave before the birth and 14 weeks after. There is no statutory right to salary, but the father may receive leave benefits from Danish authorities.

#### Paternity Leave Payment
* If you are [eligible](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave), you will receive 100% paid Parental Leave for up to 16 weeks. GitLab's Parental leave will run concurrently with any statutory leave and/or pay entitlements the team member is eligible for.

#### Parental leave
Parents are entitled to 32 weeks leave with a possibility to extend to a maximum of 46 weeks. Leave pay is due for 32 weeks given relevant conditions are met.

#### Applying for Parental Leave in Denmark
To initiate your parental leave, submit your time off by selecting the `Parental Leave` category in PTO by Roots at least 30 days before your leave starts. Please familiarize yourself with [GitLab's Parental Leave policy](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave). 

#### Other Leave Policies
* **Adoption Leave:** Adoptive parents are entitled to 4 weeks of leave prior to receiving the child, if the adoptive parents have to reside outside of Denmark to adopt. If the child is adopted in Denmark, and it is necessary to reside where the place child is situation, parents are eligible for one week of leave before the adoption. **In all cases of adoption, one of the adoptive parents is entitled to adoption leave of 14 weeks and the other with to leave with statutory benefits for 2 weeks. The remaining 12 weeks can be split between the adoptive parents.**

* **Care leave(omsorgsdage):** Intended to allow team members to care for a sick child. This is not a statutory requirement, but is common.  

## South Africa

### Healthcare Monthly Allowance

* Healthcare Monthly Allowance will be paid by Remote as an allowance to the team members for their own Medical Coverage.
* This amount will be paid on monthly basis with the regular payroll as a reimbursement.
* Proof of coverage must be shared or forwarded to total-rewards@ domain in order to receive the correct allowance amount. 
* The allowance will be up to R5,000 per month for team member and up to R8,500 per month for team member plus Dependants.
* The Total Rewards team will email the proof of coverage to Remote to have the reimbursement processed. 

### Discovery Life Provident Umbrella Fund (Provident fund match)
 
Under the Remote Technology scheme, the employer and member both pay **5% each to the Provident fund** as contributions. Applicable tax laws provide that any contribution the employer makes is treated as a contribution made by the member. The contributions will qualify for a tax deduction in each tax year of assessment. 

### Group Life including the Global Education Protector & Funeral Family Benefit

* Sum insured of the Group Life Cover will be 4 times of the annual salary.
* The primary protection offered by group life cover is as follows:
    * Provide for living expenses for surviving dependants
    * Extinguish debt
    * Protect the lifestyle of the surviving dependants
    * Fund for the education costs of the surviving children
* Upon the death of the member, spouse or child, a benefit payment equal to the amount of the Funeral Cover will be made. 
* The sum assured for this benefit is up to R30,000
* This benefit is 100% contributed and covered by Remote.    

### Disability - Income Continuation Benefit

* The Income Continuation Benefit is designed to provide team member with a payment equal to the income they received before they became disabled or severely ill.
* Disability refers to injury, illness or disease that has resulted in a member being unable to perform his or her own job based on objective medical criteria.
* Due to changes in the tax treatment of income protection benefits, the disability income benefit will be calculated on a flat **75% of member salary**, but subject to a maximum of the team members’s net of tax salary or specified rand maximum including any retirement fund waiver benefits.
* The 75% income continuation will come into effect from the **4th month** of the leave.
* This benefit is 100% contributed and covered by Remote.

### Disability - Severe Illness

* A severe illness is an illness that affects a person’s lifestyle in such a way that their ability to function normally is altered.
* Sum assured of this benefit is twice the annual salary of the team member (2 x Annual Salary).
* Discovery Life provides insurance to cover team member against the impact of a severe illness. The Severe Illness Benefit pays a lump sum if a team member is diagnosed with a covered physiological or anatomical severe illness. The claim payment is proportional to the severity of the illness, with severity levels that have been set to reflect the financial impact of the illness on their lifestyle.
* The lump sum benefit provides financial assistance to ensure that the team member can maintain their lifestyle after a life-changing event. This could mean having their homes modified to accommodate their injury or illness, or reinvesting the money to replace the monthly income they can no longer earn
* This benefit is 100% contributed and covered by the Remote.

For more details on benefits managed by Discovery: [Remote Technology Employee Benefits](https://gitlab.com/gitlab-com/www-gitlab-com/uploads/bfd6b16936b7cfaccc8fe0992bd7271f/ZA_Remote_Technology.pdf)

### Parental Leave

To initiate your parental leave, submit the dates via PTO by Roots under the Parental Leave category. This will prompt the Absence Management team to process your leave. You can find out more information about our Parental Leave policy on the [general benefits page](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave).

If you are eligible to receive parental leave from GitLab, your payments will be made up of payments from UIF and the rest from GitLab. Once you send your parental leave notification in PTO by Roots, the Absence Management team will notify Remote. They will then get in touch with you to explain the UIF payment claim process. 


## Spain

All of the benefits listed below are administered and managed by [Remote](https://remote.com/)

### Healthcare

* Remote must offer [Safety Wings](/uploads/2d3c2553f71063d495d4f540b3358d95/Benefits_Spain__3_.pdf) premium plan to all employees (this covers team members only and does not include dependants).
* Team members have the option to waive Safety wings and instead can be paid a taxable bonus of $300/€252.35 each month, to be used to cover their own medical insurance and include dependants. This will be marked as 'bonus' on the payslip. 
* Proof of coverage must be shared with total-rewards@domain

- Accruals for 13th and 14th month salaries
- General risks and unemployment insurance
- Salary guarantee fund (FOGASA)
- Work accident insurance

GitLab currently has no plans to offer Life Insurance, Private Medical cover, or a private pension due to the government cover in Spain. The [GitLab Life Insurance plan](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#life-insurance) still applies.

### Statutory Leave Policies

#### Holiday Leave

All full-time workers are legally entitled to 22 days paid holiday leave a year. In addition, full-time workers have 14 paid public holidays a year.

#### Paid Maternity leave

Employees in Spain are eligible for up to 16 weeks of Maternity leave. This rises to 18 weeks for twins and 20 weeks in the case of triplets. For children with disabilities there is an additional two weeks. 

Four out of the 16 weeks may be taken prior to the birth of the child.  At least six uninterrupted weeks must be taken immediately following the birth. Also, if you have to take time off whilst pregnant for medical reasons, you are still entitled to the 16 weeks maternity leave after your child is born.

It is also possible to use the 16 weeks in combination with a part-time working timetable or even add holiday time that you are due in order to extend your leave.

For mothers of premature babies or where a baby has to remain in hospital for more than seven days following the birth, standard maternity leave can be extended for up to 13 weeks.

During the maternity period, team members are not paid a salary, but instead a maternity benefit. This contribution is paid directly from the [Social Security Administration](https://www.seg-social.es/wps/portal/wss/internet/Conocenos/QuienesSomos/29413?changeLanguage=es) to the employee. 

Same-sex couples: both parents are entitled to paid leave. One will have to apply for paternity benefits, and the other for maternity benefits. However, in order to qualify for paid paternity (or maternity) benefits, it is essential that each parent has a legal link with the child. This means that paid leave will only be granted either: 
     * If you are a biological parent; or 
     * If you have legally adopted the child. 
     Being married to the biological or adoptive parent of a child does not qualify you for paid leave.

#### Applying for Maternity Leave
In order to be eligible for paid maternity leave, you must make sure to meet the following requirements:

* You must be working for a company, or as a self-employed worker.
* If you are below 21 years old, you won’t be required to have made contributions to social security before.
* If you are 21 to 26 years old, then you must be making contributions to social security for at least 90 days during the past 7 years, or 180 days during your entire working life.
* If you are over 26 years old, 180 days during the past 7 years or at least 360 days during your working life.
    
If you don’t meet this requirement, you can still get your maternity leave and get paid during those 16 weeks. Under certain conditions, you will get a non-contributory pension, which pays approximately 530€ per month. Team Members will need to request your [work-life certificate](https://balcellsgroup.com/employment-history-report/) at the “Tesorería de la Seguridad Social”, which can be requested online.

To help plan your leave click: [How Much Exactly Will You Get Paid On Leave](https://balcellsgroup.com/maternity-leave-spain/#How_much_exactly_will_you_get_paid_during_maternity_leave).

#### Unpaid Maternity Leave
Excedencia sin sueldo offers the right to take extended leave from work for up to a maximum of three years in total for a mother to care for her children and still return to the same employer. You can request that your exact post be held for a maximum of one year, after which you may be offered a similar post for up to 2 years.

During this absence you will still be recognized as contributing to the social security system. You do not need to agree the time period with your employer beforehand and legally you are required to give two weeks notice before returning to your post.

#### Paternity Leave
Team Members in Spain are eligible for 16 weeks of Paternity leave.  The first six weeks must be taken immediately and consecutively after the child’s birth, whereas the other 10 weeks can be taken non-consecutively during the first 12 months of the baby’s life, which can be extended by one week per child in the case of a multiple birth. An extra week’s leave can also be applied for if the baby is born with a disability or health problems. If the baby is premature or has to be hospitalized for longer than seven days, leave can be extended for up to an additional 13 weeks.

This leave must be taken in batches of one week as a minimum, but the weeks can be split over time if desired. The father is also entitled to take unpaid childcare leave until the child is three years old.

#### Paternity Leave Payments
As long as the father has correctly paid his social security contributions—for a minimum of 180 working days within the past seven years or 360 total days in his entire professional life — 100% of his salary will be covered (as long as payment does not exceed 4070.10 EUR). 

This money comes from the Spanish government, not his employer, though employers are liable for certain taxes that pertain to the salary, such as withholding.

#### Applying for Parental Leave in Spain
To initiate your parental leave, submit your time off by selecting the `Parental Leave` category in PTO by Roots at least 30 days before your leave starts. Please familiarize yourself with [GitLab's Parental Leave policy](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave).

#### Breastfeeding leave
Parents can take an additional breastfeeding leave after parental leave. This breastfeeding leave can be enjoyed in two ways:
- take one hour off during the day, until the child is nine months old (it can either be one hour, or half an hour at the beginning and at the end of the working day)
- take 15 consecutive days after the parental leave

The breastfeeding leave is considered as working time, meaning that the employee doesn't receive anything from Social Security and is paid by the company at 100%.

### Other leave policies
* **Adoption leave:** upon adoption of a child or have taken in a foster child, employees are entitled to the same entitles apply as for maternity and paternity rights. Applies to both parents.  When adopting or fostering a child, each parent is eligible for the same 16 weeks as is the biological parent if the child is under six years old. If the child is older, both adoptive parents are eligible for the remaining optional 10 weeks that a biological parent would have after the first obligatory six weeks after birth.

* **Emergency and short absense leave:** intended for unforeseen personal circumstances for which an employee has to take time off immediately. Examples include making arrangements for the care of a sick family member or in the event of a death in the family. Up to two calendar days or four if travel is required.

* **Long-term care leave:** when a child, partner or parent of the employee is seriously (i.e. life threateningly) ill and requires care, the employee can request long-term care leave.

* **Unpaid leave:** the employee may take unpaid leave in consultation with the employer on a full-time or part-time basis. Granted at the discretion of the employer, but must always be set in an individual or collective agreement. Unpaid leave periods are not regulated by law.

## Italy

All of the benefits listed below are administered and managed by [Remote](https://remote.com/)

### Health Insurance

Team members also have the option to select Remotes Safety Wings premium medical plan (for team members only). If they wish to add dependants this will be deducted through the Payroll (usd) $154/mo

* Team members have access to government-subsidized healthcare as well as additional insurance called QU.A.S. The annual cost for QU.A.S. is 56 EUR paid by the team member and 350 EUR paid by the company.
* Pension and Life Insurance - As part of Remote payroll, mandatory contributions are made towards social security funds. Remote currently does not offer a supplementary pension but will be reviewing this in the upcoming year. The [GitLab Life Insurance plan](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#life-insurance) still applies.

### Statutory Leave Policies

#### Holiday leave
All full-time workers are legally entitled to a minimum of four week's paid holiday leave a year. In addition, full-time workers have 12 public holidays a year. Individual contracts can provide for a longer period of holiday entitlement and holidays cannot be replaced by a payment in lieu. Amount of holiday leave also depends on the national collective agreement that the employment contract is under.

#### Pregnancy & Maternity leave
In Italy, maternity leave is compulsory and women must take up to two months off from work before her due date and three months off after the baby's birth. In certain scenarios, women may ask their employers for more time off before the baby's due date if her pregnancy is considered at risk and if her workplace puts her health or her baby's in danger. In some instances, women can request to work up to one month before her due date with the approval of her doctor and employer. However, this means that she will have to take a total of four months off after the birth of her child

#### Maternity Leave Payments
These pensions are funded by the INPS (National Institute for Social Security) which are financed by the employers and employees through the rate established by the employment sector. Workers can either be paid from INPS or through the employer who is compensated by the INPS. During the five-month leave, women are paid up to 80% of their wage. 

If eligible, a team member's first 16 weeks of leave will be supplemented the remaining 20% per GitLab's [Parental Leave policy](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave).  GitLab's Parental leave will run concurrently with any statutory leave and/or pay entitlements the team member is eligible for.

#### Applying for Maternity Leave in Italy
To initiate your parental leave, submit your time off by selecting the `Parental Leave` category in PTO by Roots at least 30 days before your leave starts. Please familiarize yourself with [GitLab's Parental Leave policy](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave).

#### Paternity Leave
Fathers are also obligated to take a seven-day paid leave of absence from work during the five months of maternity leave. The mother has the option to extend the father's paternal leave if she transfers one of her maternity days to him.

If eligible, team members can take up to 16 weeks of time away (in total) per GitLab's [Parental Leave policy](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave).  GitLab's Parental leave will run concurrently with any statutory leave and/or pay entitlements the team member is eligible for.

#### Applying for Paternity Leave in Italy
To initiate your parental leave, submit your time off by selecting the `Parental Leave` category in PTO by Roots at least 30 days before your leave starts. Please familiarize yourself with [GitLab's Parental Leave policy](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave).

#### Parental Leave
In addition to maternity and paternity leave, parents can take extended unpaid parental leave for up to 10 months. For a mother this can be up to 6 months in addition to the maternity leave.  Parental leave allowance is significantly less than maternity leave allowance; only 30% of one's wages, and is paid through the INPS.

#### Other leave policies
* **Adoption leave:** upon adoption of a child, employees are entitled to 3 months of maternity or paternity leave and employees are entitled to the same financial benefits of natural children. Parents can also take parental leave in the first three years the child is in the family for same periods and financial benefits. Applies to both parents

* **Work-related Injust leave:**  collective bargaining agreements or individual contracts generally provide for a period paid time off in the caseof work injury. The period is generally between 6 and 12 months and applies for both a single period of sick leave and multiple periods. The employee is entitled to keep their job and receive their salary in proportion to the period set out in the collective bargaining agreement or individual employment contract.

## Switzerland

All of the benefits listed below are administered and managed by [Remote](https://remote.com/)

As part of the social security obligations, for its part, the employer pays:

* Half of the Unemployment Insurance
* The whole of the Non Occupational Accident insurance contribution (professional)
* The whole of the Occupational Accident insurance contribution (professional)
* Half of the Supplementary Unemployment insurance (if over a certain value).
* Half of the Old age, survivors disability insurance
* The whole of the Family Compensation Fund
* All Administrative fees
* Pension plan: [AXA Basisvorsorge AG J25](https://docs.google.com/document/d/1OlFzuWWJQfw5wOEr6hq0yGM4JoNoxjZTzKIFct2FlEQ/edit), per retirement credits

### Statutory Leave Policies

#### Paid Time Off
Employees 20 years old and above receive at least four weeks of paid time off per year. Employees younger than 20 years old are entitled to five weeks. Employees are also entitled to paid time off for public holidays in the canton in which they live.

#### Pregnancy and maternity leave
Birthing parents in Switzerland are eligible for 14 weeks (98 days) of maternity leave that begins when the child is born.  Team members are prohibited from working for the first 8 weeks after birth.  

#### Maternity Leave Pay
Team members are eligible for up to 80% of their wages during leave; capped at CHF 196 per day.     
    * Employees in the Geneva canton receive 16 weeks. 

Employees must contribute to AHV (social security) for the nine months preceding childbirth and must be actively employed for five months preceding childbirth to be eligible. 

If eligible, a team member's leave pay will be supplemented the remaining 20% per GitLab's [Parental Leave policy](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave). GitLab's Parental leave will run concurrently with any statutory leave and/or pay entitlements the team member is eligible for.

#### Applying for Maternity Leave in Switzerland
To initiate your parental leave, submit your time off by selecting the `Parental Leave` category in PTO by Roots at least 30 days before your leave starts. Please familiarize yourself with [GitLab's Parental Leave policy](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave).  

#### Partner/Paternity leave
Since 1 January 2021, new fathers can take two weeks of paid paternity leave (14 days’ daily allowance). 

If eligible, team members can take up to 16 weeks of time away (in total) per GitLab's [Parental Leave policy](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave).  GitLab's Parental leave will run concurrently with any statutory leave and/or pay entitlements the team member is eligible for.

#### Paternity Leave Pay
Paternity allowance amounts to 80% of earnings up to a maximum of CHF 196 per day. Cantonal provisions, personnel regulations, and collective employment contracts may provide for more generous solutions.

If eligible, a team member's leave pay will be supplemented the remaining 20% per GitLab's [Parental Leave policy](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave). GitLab's Parental leave will run concurrently with any statutory leave and/or pay entitlements the team member is eligible for.

#### Sick leave

Employees are entitled to ongoing payments during sick leave, depending on how long they have worked for the company. Typically, employees receive three weeks of sick leave during the first year. Employers commonly have benefits insurance schemes in lieu of sick leave, under which employees can receive 80% of their most recent salary for up to 720 days.

## Mexico

### Social Security

All workers employed in Mexico must be registered with and contribute to the following institutions that deal with different social security insurance benefits:
* Mexican Institute of Social Security (IMSS):
    * Provides medical services, child day care, accident and sickness compensation, pregnancy benefits and disability pensions
* National Workers’ Housing Fund Institute (INFONAVIT):
    * Provides subsidized housing to employees as well as loans, and the Retirement Savings Program (SAR). SAR provides employees with retirement benefits when they reach 65 years of age.

Employees in Mexico are covered by the Social Security Law, under IMSS who is responsible for administering social security insurance benefits and the collection of contributions.
* Both the employer and employee are required to contribute to social security, although the employer has the responsibility of withholding the employee’s contribution.
* These contributions fund retirement pensions, health and maternity insurance, occupational risk, day-care, disability and life insurance, and unemployment/old age insurance.

### Medical Benefits

* Healthcare Monthly Allowance will be paid by Remote as an allowance to the team members.
* The allowance will be 5000 USD per annum. 
* This amount will be paid on monthly basis with the regular payroll as a reimbursement.

### Life Insurance

* Life insurance cover provided via Remote (Details to be added by Total Rewards)

### Christmas Bonus (Aguinaldo)

* GitLab offers 30 working days pay (which includes total earnings + taxable allowances + commissions)
* Paid by December 20th (so the employee can use it for the holiday)
* Employees with less than one year of service will recieve a pro-rated christmas bonus. 

### Vacation Bonus (Prima)

The vacation premium is an additional cash benefit given to employees for use on their vacation. It is calculated as a minimum of 25% of daily salary multiplied by the number of days of vacation. Employees who have provided one year of service must be afforded a minimum of 6 paid vacation days in their first year of employment. Two working days will be added to that vacation time every following year through the fourth year. After five years, employers are required to add two days of vacation time every five years.
* Here is a chart on how vacations days increase:

| Year(s)  | Days |
|----------|------|
| 1        | 6    |
| 2        | 8    |
| 3        | 10   |
| 4        | 12   |
| 5 to 9   | 14   |
| 10 to 14 | 16   |
| 14 to 19 | 18   |

**Calculation:**
* To calculate the vacation bonus based on 25% and a salary of $50,000 USD per year:
* $50,000 USD divided by 365 days = $136.99 USD daily pay rate
* $136.99 USD daily pay rate multiplied by 25% = $34.25 USD
* $34.25 USD multiplied by 6 vacation days (first year) = $205.50 USD vacation bonus
* It can be paid right away when the vacation is taken, or as a lump sum upon completing one year of service (or employment ends). It is the employer’s choice which method to use.
* Unused leave must also be paid when employment ends (in other words, the employee does not ‘forfeit’ unused vacation time)

### Mexico Parental Leave

#### Statutory General Entitlement 

**Maternity Leave:** Team members can take 12 weeks of Maternity Leave (6 weeks before the child is born and 6 weeks after birth). 

**Paternity Leave:** Team members can take 5 days of Paternity Leave.

#### Maternity Leave Payment

* 12 weeks of the team member's Maternity Leave will be paid by the Mexican Social Security Institute (MSSI). 
* If [eligible](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave), GitLab will supplement the MSSI payments to ensure the team member receives 100% pay for up to 16 weeks of leave. 

#### Paternity Leave Payment

* If [eligible](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave), the team member will receive 100% paid Parental Leave from GitLab for up to 16 weeks.

#### Applying for Parental Leave in Mexico
To initiate your parental leave, submit your time off by selecting the `Parental Leave category` in PTO by Roots at least 30 days before your leave starts. Please familiarize yourself with [GitLab's Parental Leave policy](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave).


### Mexico Sick Leave

Team members unable to work because of a nonwork-related injury or illness and who have made payments into the social security system for the four weeks before the condition developed are eligible for paid sick leave through the Social Security Institute. The benefit, which is 60% of an employee’s regular wage, is paid from the fourth day of the illness for up to 52 weeks and maybe extended for another 52 weeks. 

## Hungary

### Social Security

The Hungarian Social Security Act has employer and team member contributions to cover statutory requirements such as pension, health insurance, and unemployment. 

### Medical

GitLab does not plan to offer Private Health Insurance at this time because team members in Hungary can access the public health system for free or at a lower cost through the Hungarian state healthcare system. This system is funded by the National Health Insurance Fund.

### Pension

GitLab does not plan to offer pension benefit at this time as the Hungarian pension system provides for a minimum pension, with a qualifying condition of minimum 20 years of service, of HUF 28,500 per month. If the average contribution base is less than the amount of the minimum pension, the pension will equal 100% of the average monthly wage.

### Life Insurance

GitLab does not plan to offer life insurance at this time as team members can access the benefits from [Social insurance system](https://tcs.allamkincstar.gov.hu/) if they get ill, injured or have a disability.

### Hungary Parental Leave

#### Statutory General Entitlement

**Maternity Leave:** Team members can take up to 24 weeks of Maternity Leave. The leave must start 4 weeks prior to the scheduled due date.

**Paternity Leave:** Team members can take 5 days of Paternity Leave or 7 days in case of twins.

**Parental Leave:** Team members can take unpaid leave to care for their child until the child reaches the age of 3 or until the child reaches the age of 10 if the team member receives child care allowance. Team members are also entitled to extra vacation days based on the number of children they have: 

* two working days for one child; 
* four working days for two children; 
* a total of seven working days for more than two children under sixteen years of age.

#### Matenity Leave Payment

* The team member will receive Pregnancy and Confinement Benefit (CSED) at a rate of 70% of their salary for 24 weeks.
* If [eligible](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave), GitLab will supplement the CSED payments to ensure the team member receives 100% pay for up to 16 weeks. 

#### Paternity Leave Payment

* The team member will receive payment from the Hungarian State Treasury for 5 days of their leave.
* If [eligible](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave), GitLab will supplement the State payments to ensure the team member receives 100% pay for up to 16 weeks. 


## Austria

### Social Security

The Austrian Social Security Act General Social Insurance Act (Allgemeines Sozialversicherungsgesetz, ASVG) has employer and team member contributions to cover statutory requirements such as pension, health insurance, accident insurance and unemployment. 

### Medical

GitLab does not plan to offer Private Health Insurance at this time because team members in Austria can access the public Austrian health insurance system. This health insurance scheme covers all the team members and their family members.  

### Pension

GitLab does not plan to offer pension benefit at this time as Austria has their uniform pension system through The Act on the Harmonisation of Austrian Pension Systems. The amount of a pension is calculated on the basis of the duration of the pension insurance and amount of the contributions paid. In order to receive the pension a team member must have paid contributions for at least 180 months (15 years).

### Life Insurance

GitLab does not plan to offer life insurance at this time as team members can access the benefits from Social insurance system if they get ill, injured or have a disability.

###  Bonus Payment

The salary will be paid paid 14 times a year. This includes 12 months salary and two bonuses. 

### Remote Technology - Austria Leave Policy

#### Statutory Maternity Leave

The statutory entitlement for maternity leave (Mutterschaftsurlaub) is 16 weeks. The leave must start **8 weeks prior to the scheduled delivery date**. For high-risk births, leave after the birth can be extended to 12 weeks. 

#### Statutory Parental Leave

Mothers and fathers are entitled to parental leave until the child reaches the age of 24 months (maximum), provided the parent in parental leave lives in the same household as the child. The minimum period of the parental leave is two months. The dismissal and termination protection ends four weeks after the end of the parental leave.

During the time of parental leave, and provided the conditions are satisfied, parents are entitled to childcare allowance(Kinderbetreuungsgeld) under the Child Care Payment Act from social security. 

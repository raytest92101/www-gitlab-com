### Engineering Allocation

Engineering is the DRI for mid/long term team efficiency, performance, security (incident response and anti-abuse capabilities), availability, and scalability. The expertise to proactively identify and iterate on these is squarely in the Engineering team. Whereas Product can support in performance issues as identified from customers. In some ways these efforts can be viewed as risk-mitigation or revenue protection. They also have the characteristic of being larger than one group at the stage level. Development would like to conduct an experiment to focus on initiatives that should help the organization scale appropriately in the long term.  We are treating these as a percent investment of time associated with a stage or category. The percent of investment time can be viewed as a prioritization budget outside normal Product/Development assignments.

Engineering Allocation is also used in short-term situations in conjunction and in support of maintaining acceptable Error Budgets for GitLab.com and our [GitLab-hosted first](/direction/#gitlab-hosted-first) theme.

Unless it is listed in this table, Engineering Allocation for a stage/group is 0% and we are following normal [prioritization](/handbook/product/product-processes/#prioritization). Refer to this [page](https://about.gitlab.com/handbook/engineering/engineering-allocation/) for Engineering Allocation charting efforts. Some stage/groups may be allocated at a high percentage or 100%, typically indicating a situation where all available effort is to be focused on Reliability related (top 5 priorities from [prioritization table](/handbook/product/product-processes/#prioritization)) work.

Mid/long term initiatives are engineering-led. The EM is responsible for recognizing the problem, creating a satisfactory goal with clear success criteria, developing a plan, executing on a plan and reporting status.  It is recommended that the EM collaborate with PMs in all phases of this effort as we want PMs to feel ownership for these challenges.  This could include considering adding more/less allocation, setting the goals to be more aspirational, reviewing metrics/results, etc.   We welcome strong partnerships in this area because we are one team even when allocations are needed for long-range activities.

During periods of Engineering Allocation, the PM remains the interface between the group and the fields teams & customers. This is important because:
- It allows Engineering to remain focused on the work at hand
- It maintains continuity for the field teams - they should not have to figure out different patterns of communication for the customer
- It keeps PMs fully informed about the product's readiness

| Group/Stage | Description of Goal | Justification | Maximum % of headcount budget | People | Supporting information | EMs / DRI | PMs |
| ------ | ------ | ------- | ------ | ------ | ------- |  ------ | ------ |
| Manage:Authentication and Authorization (BE) | floor % | empower every SWEs from raising reliability and security issues | 33% | 1 | N/A | @m_gill | @hsutor |
| Manage:Authentication and Authorization (BE)  | 3 month headcount reset to help Manage:Workspace | 3 month headcount reset to help Manage:Workspace |  25% | 1 | 3 month headcount reset to help Manage:Workspace | @lmcandrew  | @hsutor   |
| Manage:Workspace (BE)  | Scalability of GitLab hierarchy functionality (Workspace) | Reduce duplication of code and increase performance for Groups/Projects |  50% | 1 | [Consolidate Groups and Projects](https://gitlab.com/groups/gitlab-org/-/epics/6473) | @mksionek  | @mushakov |
| Manage:Workspace (BE)  | Linear Namespace Queries | Replace recursive CTE queries, which are complex and unpredictable |  50% | 1 | [Linear Namespace Queries](https://gitlab.com/groups/gitlab-org/-/epics/5296) | @mksionek | @ogolowinski  |
| Manage:Import (BE)  | Improving Error Budget and Infra-Dev Issues |  Backlog of production incidents tied to infradev | 100% | 1 | [Infradev & Security Issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&state=opened&label_name[]=group%3A%3Aimport&label_name[]=Engineering%20Allocation) | @wortschi |  @hdelalic  |
| Manage:Compliance (BE) | floor % | empower every SWEs from raising reliability and security issues | 33% | 1 | N/A | @dennis | @stkerr |
| Manage:Optimize (BE) | floor % | empower every SWEs from raising reliability and security issues | 33% | 1 | N/A | @m_gill | @hsnir1 |
| Plan:Project management | 3 month headcount reset to help Manage:Workspace | 3 month headcount reset to help Manage:Workspace | 25% | 4 | 3 month headcount reset to help Manage:Workspace | @jlear | @gweaver |
| Plan:Product Planning | 3 month headcount reset to help Manage:Workspace | 3 month headcount reset to help Manage:Workspace | 20% | 5 | 3 month headcount reset to Manage:Workspace | @johnhope | @cdybenko |
| Create:Source Code (BE) | floor % | empower every SWEs from raising reliability and security issues | 10% | 1 | N/A | @sean_carroll | @sarahwaldner  |
| Create:Code Review (BE) | floor % | empower every SWEs from raising reliability and security issues | 10% | 1 | N/A | @mnohr | @phikai  |
| Create:Editor (BE) | Infradev, Linear Queries | Close all infradev issues, work through linear queries epic | 50% | 2 | [Overview](https://gitlab.com/gitlab-org/gitlab/-/issues/341883) | @oregand | @ericschurter |
| Create:Gitaly | Infra-Dev Issues, P1/S1 issues, security issues and Customer Escalations (engineering approved) | Improve reliability of Gitaly |  20%  | 6 |  [Infradev Issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&state=opened&label_name[]=infradev&label_name[]=group%3A%3Agitaly) | @timzallmann | @mjwood |
| Verify:Pipeline Execution (BE) | Burndown of sharding-blockers, infradev and Security issues | Support Data Decomposition efforts, continue supporting Reliability in the Veriy stage | 100% | 3 | [Verify priorities proposal for FY22-Q4](https://gitlab.com/gitlab-org/verify-stage/-/issues/145) | @cheryl.li |  @jheimbuck_gl   |
| Verify:Pipeline Authoring (BE) | Burndown of sharding-blockers, infradev and Security issues | Support Data Decomposition efforts, continue supporting Reliability in the Veriy stage | 66% | 2 | [Verify priorities proposal for FY22-Q4](https://gitlab.com/gitlab-org/verify-stage/-/issues/145) | @cheryl.li |  @dhershkovitch  |
| Verify:Pipeline Authoring (BE) | floor % | empower every SWEs from raising reliability and security issues | 10% | 3 | N/A | @marknuzzo | @dhershkovitch |
| Verify:Runner | floor % | empower every SWEs from raising reliability and security issues |  10% | 6 | N/A | @erushton | @DarrenEastman |
| Verify:Pipeline Insights (BE) | Burndown of sharding-blockers, infradev and Security issues | Support Data Decomposition efforts, continue supporting Reliability in the Veriy stage | 100% | 2 | [Verify priorities proposal for FY22-Q4](https://gitlab.com/gitlab-org/verify-stage/-/issues/145) | @cheryl.li |  @jreporter   |
| Package:Package | floor % | empower every SWEs from raising reliability and security issues | 10% | 5 | N/A | @dcroft | @trizzi |
| Release:Release | 3 milestones manage:Import Headcount Reset | Unlocks a new CEO initiative | 17% | 1 | https://gitlab.com/gitlab-com/Product/-/issues/3062 | @nicolewilliams | @cbalane |
| Configure:Configure | 3 milestones manage:Import Headcount Reset | Unlocks a new CEO initiative  | 20% | 1 | https://gitlab.com/gitlab-com/Product/-/issues/3062 | @nicholasklick | @nagyv-gitlab |
| Verify | Improve GitLab.com CI capacity visibility and Shared Runners ability to handle increased load | Give us 4-5 years of runway | 20% | 3 | [CI Scaling Target](https://gitlab.com/groups/gitlab-org/-/epics/5745), [CI/CD Capacity Planning](https://gitlab.com/groups/gitlab-org/-/epics/6927) | @grzesiek  | @jreporter |
| Secure:Static Analysis | floor % | empower every SWEs from raising reliability and security issues | 10% | 5 | N/A | @twoodham | @connorgilbert |
| Secure:Dynamic Analysis | floor % | empower every SWEs from raising reliability and security issues | 10% | 5 | N/A | @sethgitlab | @derekferguson |
| Secure:Composition Analysis | Proposed 3 month headcount reset to help manage:Import | Proposed 3 month headcount reset to help manage:Import | 25% | 4 | Proposed 3 month headcount reset to help manage:Import | @gonzoyumo  | @NicoleSchwartz |
| Secure:Threat Insights | Bring error budget back to green | Work on backlog of reliability and security issues | 25% | 4 | [List of issues](https://gitlab.com/gitlab-org/gitlab/-/issues/299275#saas-reliability-focus-for-q3fy22) | @thiagocsf | @matt_wilson |
| Protect:Container Security | floor % | empower every SWEs from raising reliability and security issues | 10% | 5 | N/A | @thiagocsf | @sam.white |
| Growth:Activation | floor % | empower every SWEs from raising reliability and security issues | 10% | 1 | N/A | @jayswain | @jstava |
| Growth:Conversion | floor % | empower every SWEs from raising reliability and security issues | 10% | 2 | N/A | @kniechajewicz | @s_awezec |
| Growth:Expansion | 3 month headcount reset to help manage: Import | 3 month headcount reset to help manage: Import | 50% | 2 | 3 month headcount reset to help manage: Import | @pcalder | @gdoud |
| Growth:Adoption | floor % | empower every SWEs from raising reliability and security issues | 10% | 2 | N/A | @jayswain | @mkarampalas |
| Growth:Product Intelligence | floor % | empower every SWEs from raising reliability and security issues | 10% | 6 | N/A | @nicolasdular | @amandarueda |
| Fulfillment | Improve availability of CustomersDot by migrating from Azure to GCP | Improve availability of CustomersDot due to [several recent outages](https://gitlab.com/gitlab-com/gl-infra/production/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Service%3A%3ACustomers) | 20% (4 Fulfillment Engineers + 0.67 Infrastructure Engineers) | 20 | [Epic](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/390) | @jeromezng | @justinfarris |
| Enablement:Distribution | floor % | empower every SWEs from raising reliability and security issues | 10% | 9 | N/A | @mendeni | @dorrino |
| Enablement: Geo | 3 months headcount reset to new staging environment | 3 months headcount reset to new staging environment | 13% | 7 | 3 months headcount reset to new staging environment | @nhxnguyen | @nhxnguyen |
| Enablement:Database | Primary Key overflow, Retention Strategy, Schema Validation, migration improvements, testing | Database has been under heavy operational load and needs improvement | 100% | 5 | [Automated Migration testing](https://gitlab.com/groups/gitlab-org/database-team/-/epics/6), [Automated migrations for primary key conversions](https://gitlab.com/groups/gitlab-org/-/epics/5654), [Remove PK overflow](https://gitlab.com/groups/gitlab-org/-/epics/4785), [Schema Validation](https://gitlab.com/groups/gitlab-org/-/epics/3928), [Reduce Total size of DB](https://gitlab.com/groups/gitlab-org/-/epics/4181) | @craig-gomes | TBD |
| Enablement:Sharding | floor % | empower every SWEs from raising reliability and security issues | 10% | 4 | N/A | @craig-gomes | @fzimmer |
| Quality:Ops QE | Improve Staging environment | Improving reliability & availability is 3rd priority in [Prioritizing technical decisions](https://about.gitlab.com/handbook/engineering/principles/#prioritizing-technical-decisions) | 10% | 1 | [New staging epic](https://gitlab.com/groups/gitlab-org/-/epics/6401) | @vincywilson | TBD |
| Quality:Enablement QE | Improve Staging environment | Improving reliability & availability is 3rd priority in [Prioritizing technical decisions](https://about.gitlab.com/handbook/engineering/principles/#prioritizing-technical-decisions) | 10% | 1 | [New staging epic](https://gitlab.com/groups/gitlab-org/-/epics/6401) | @vincywilson | TBD |
| Infrastructure:Delivery | Improve Staging environment | Improving reliability & availability is 3rd priority in [Prioritizing technical decisions](https://about.gitlab.com/handbook/engineering/principles/#prioritizing-technical-decisions) | 80% | 2 | [New staging epic](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/559) | @amyphillips | TBD |
| Infrastructure:Reliability | Improve Staging environment | Improving reliability & availability is 3rd priority in [Prioritizing technical decisions](https://about.gitlab.com/handbook/engineering/principles/#prioritizing-technical-decisions) | 80% | 3 | [New staging epic](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/559) | @amyphillips | TBD |
| Enablement:Geo | Improve Staging environment | Improving reliability & availability is 3rd priority in [Prioritizing technical decisions](https://about.gitlab.com/handbook/engineering/principles/#prioritizing-technical-decisions) | 10% | 1 | [New staging epic](https://gitlab.com/groups/gitlab-org/-/epics/6401) | @nhxnguyen | TBD |

#### Broadcasting and communication of Engineering Allocation direction

Each allocation has a [direction page](/handbook/product/product-processes/#managing-your-product-direction) maintained by the Engineering Manager. The Engineering Manager will provide regular updates to the direction page. Steps to add a direction page are:

1. Open an MR to the [direction content](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/direction/)
1. Add a directory under the correct stage named for the title Engineering Allocation
1. Add a file for the page named `index.html.md` in the newly created directory

To see an example for an Engineering Allocation Direction page, see [Continuous Integration Scaling](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/source/direction/verify/continuous_integration_scaling/index.html.md). Once the Engineering Allocation is complete, delete the direction page.

#### Communicating Engineering Allocation Progress

Groups allocating effort to an engineering allocation should update progress synchronously or asynchronously in the weekly, cross-functional infradev and engineering allocation meeting [[agenda](https://docs.google.com/document/d/1j_9P8QlvaFO-XFoZTKZQsLUpm1wA2Vyf_Y83-9lX9tg/edit#bookmark=id.tr8ld1ht454z) (internal)]. The intention of this meeting is to communicate progress on engineering allocations and to evaluate and prioritise escalations from infrastructure.

Engineering Allocation progress reports should appear in the following format:

1. DRI person: Short engineering allocation description (including link if possible) **Not Verbalized**
   1. Target date:
   1. Percent done:
   1. Context:
      - incident link
      - RCA link
      - Epic/meta-issue link
      - Other context
   1. Status: **Verbalized**
      - On pace (confidence?)
      - next steps / blockers / significant changes and/or wins

#### How to get a effort added to Engineering Allocation

One of the most frequent questions we get as part of this experiment is "How does a problem get put on the Engineering Allocation list?".  The short answer is someone makes a suggestion and we add it.  Much like everyone can contribute, we would like the feedback loop for improvement and long terms goals to be robust.  So everyone should feel the empowerment to suggest an item at any time.

To help with getting items that on the list for consideration, we will be performing a survey periodically.  The survey will consist of the following questions:

1. If you were given a % of engineering development per release to work on something, what would it be?
1. How would you justify it?

We will keep the list of questions short to solicit the most input.  The survey will go out to members of the Development, Quality, Security.  After we get the results, we will consider items for potential adding as an Engineering Allocation.


#### Closing out Engineering Allocation items

Once the item's success criteria are achieved, the Engineering Manager should consult with counterparts to review whether the improvements are sustainable. Where appropriate, we should consider adding monitoring and alerting to any areas of concern that will allow us to make proactive prioritizations in future should the need arise. The Engineering Manager should close all related epics/issues, reset the allocation in the above table to the floor level, and inform the Product Manager when the allocated capacity will be available to return their focus to product prioritizations.

When reseting a groups Engineering Allocation in the table above, the goal should be set as `floor %`, the goal should be `empower every SWEs from raising reliability and security issues`, percentange of headcount allocated should be `10%`, and `N/A` in place of a link to the Epic.

All engineering allocation closures should be reviewed and approved by the [VP of Development](https://about.gitlab.com/handbook/engineering/development/#team-members).

---
layout: secure_and_protect_direction
title: "Category Direction - Container Host Security"
description: "Container Host Security (CHS) refers to the ability to detect, report, and respond to attacks on containerized infrastructure and workloads."
canonical_path: "/direction/protect/container_host_security/"
---

- TOC
{:toc}

## Protect

| | |
| --- | --- |
| Stage | [Protect](/direction/protect/) |
| Maturity | [Minimal](/direction/maturity/) |
| Content Last Reviewed | `2022-02-02` |

### Introduction and how you can help
<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->

<!--
<EXAMPLE>
Thanks for visiting this category direction page on Snippets in GitLab. This page belongs to the [Editor](/handbook/product/categories/#editor-group) group of the Create stage and is maintained by <PM NAME>([E-Mail](mailto:<EMAIL@gitlab.com>) [Twitter](https://twitter.com/<TWITTER>)).

This direction page is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=snippets) and [epics]((https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=snippets) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for snippets, we'd especially love to hear from you.
</EXAMPLE>
-->
Thanks for visiting this category direction page on Container Host Security in GitLab. This page belongs to the Container Security group of the Protect stage and is maintained by Sam White ([swhite@gitlab.com](mailto:<swhite@gitlab.com>)).

### Overview
<!-- A good description of what your category is today or in the near term. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Provide enough context that someone unfamiliar
with the details of the category can understand what is being discussed. -->
Container Host Security (CHS) refers to the ability to detect, report, and respond to attacks on containerized infrastructure and workloads. Techniques include use of one or more types of intrusion detection systems (IDS) to detect attacks.  The IDS may be supplemented with custom-built monitoring capabilities and/or behavior analytics to improve the efficacy and scope of detected attacks.

An IDS is a device or software application that monitors a network or systems for malicious activity or policy violations. Malicious activity can then be reported back to an Administrator either through GitLab or through a security information and event management (SIEM) system. IDS types range in scope from single computers to large networks. The most common classifications are network intrusion detection systems (NIDS) and host-based intrusion detection systems (HIDS). Some leverage honeypots to attract and characterize malicious traffic. Some strictly leverage signature-based detection, while others use machine learning to automatically detect anomalies.

An ideal Container Host Security solution would include all types of intrusion detection systems to provide defense-in-depth and protection against a wide range of attacks.  Additional analytics can be layered on top of the data collected from an IDS to help filter out false positives and to recommend new rules to reduce false negatives.

#### Target Audience
<!--
List the personas (https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas#user-personas) involved in this category.

Look for differences in user's goals or uses that would affect their use of the product. Separate users and customers into different types based on those differences that make a difference.
-->
* [Sam (Security Analyst)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sam-security-analyst) and [Alex (Security Operations Engineer)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#alex-security-operations-engineer) are our primary target personas for any organizations that have an established security team
* [Devon (DevOps Engineer)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer) could be a secondary persona for organizations without established security teams.  This needs to be researched more.

### Where we are Headed
<!--
Describe the future state for your category.
- What problems are we intending to solve?
- How will GitLab uniquely address them?
- What is the resulting benefits and value to users and their organizations?

Use narrative techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is at least minimally realized. In order to challenge your level of ambition
(with the goal to make it sufficiently high), link to the current market leaders long-term vision and address how
we plan to displace them. -->

This category and all functionality related to it has been deprecated in the GitLab 14.8 release and is scheduled for removal in the GitLab 15.0 release.  Users who need a replacement for the functionality previously offered by this category are encouraged to evaluate the following open source projects as potential solutions that can be installed and managed outside of GitLab: [AppArmor](https://gitlab.com/apparmor/apparmor), [Falco](https://github.com/falcosecurity/falco), [FluentD](https://github.com/fluent/fluentd), Pod Security Admission](https://kubernetes.io/docs/concepts/security/pod-security-admission/).

    As part of this change, the following specific capabilities within GitLab are now deprecated and are scheduled for removal in GitLab 15.0:

    - The ability to manage integrations with the following technologies through GitLab: AppArmor, Falco, FluentD, and Pod Security Policies.
    - All APIs related to the above functionality